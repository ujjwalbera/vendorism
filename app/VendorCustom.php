<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class VendorCustom extends Model
{
    use Notifiable;


    protected $table = 'customer_address_vendor_mapping';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'customer_id', 'customer_address_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];


    /**
    * Get the user record associated with the admin.
    */
    public function customer_address_vendor_mapping()
    {
        return $this->belongsToMany('Vendorism\Customer\Models\Customer', 'customer_id');
    }
}
