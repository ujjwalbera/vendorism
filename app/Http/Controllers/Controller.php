<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public  $extraData = null;
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct(/*Role $role*/)
    {
        //$this->role = $role;
        $this->admins = 'admins';
        $this->adminProfiles = 'admin_profiles';
    }
}
