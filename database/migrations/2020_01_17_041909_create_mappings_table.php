<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_vendor_carrier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id')->unsigned();
            $table->bigInteger('carrier_id')->unsigned();
            $table->boolean('vendor_status')->default(false);
            $table->boolean('carrier_status')->default(false);
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->foreign('carrier_id')->references('id')->on('carriers')->onDelete('cascade');
            $table->index(['vendor_id', 'carrier_id']);
            //$table->unique('vendor_id', 'carrier_id');
            //$table->primary(['vendor_id', 'carrier_id']);
        });

        Schema::create('map_cust_addr_carrier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cust_addr_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('carrier_id')->unsigned();
            $table->boolean('carrier_status')->default(false);
            $table->foreign('cust_addr_id')->references('id')->on('customer_address')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('carrier_id')->references('id')->on('carriers')->onDelete('cascade');
            $table->index(['carrier_id', 'customer_id', 'cust_addr_id']);
            //$table->unique('carrier_id', 'customer_id', 'cust_addr_id');
            //$table->primary('carrier_id', 'customer_id', 'customer_address_id');
        });

        Schema::create('map_cust_addr_vendor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cust_addr_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('vendor_id')->unsigned();
            $table->boolean('vendor_status')->default(false);
            $table->foreign('cust_addr_id')->references('id')->on('customer_address')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->index(['vendor_id', 'customer_id', 'cust_addr_id']);
            //$table->unique('vendor_id', 'customer_id', 'cust_addr_id');
            //$table->primary('vendor_id', 'customer_id', 'customer_address_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_carrier_mapping');
        Schema::dropIfExists('customer_address_carrier_mapping');
        Schema::dropIfExists('customer_address_vendor_mapping');
    }
}
