<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () {

    Route::get('vendor/registration', '\Vendorism\Vendor\Http\Controllers\VendorsController@registration')->name('vendor-registration');
    Route::post('vendor/registration', '\Vendorism\Vendor\Http\Controllers\VendorsController@doRegistration')->name('vendor-doRegistration');

    Route::get('vendor/login', '\Vendorism\Vendor\Http\Controllers\VendorsController@login')->name('vendor-login');
    Route::get('vendor', '\Vendorism\Vendor\Http\Controllers\VendorsController@login')->name('vendor-login');
    Route::post('vendor/login', '\Vendorism\Vendor\Http\Controllers\VendorsController@doLogin')->name('vendor-doLogin');

    Route::get('vendor/logout', '\Vendorism\Vendor\Http\Controllers\VendorsController@doLogout')->name('vendor-logout');

    /*Route::get('vendor/forgot', function () {
        return view('vendor::forgot');
    })->name('vendor-forgot');*/
    Route::get('vendor/forgot', 'Vendorism\Vendor\Http\Controllers\VendorsController@forgot')->name('vendor-forgot');
    Route::post('vendor/forgot', 'Vendorism\Vendor\Http\Controllers\VendorsController@doforgot')->name('vendor-doforgot');

    /*Route::get('/admin/registration', function () {
        return view('admin::registration');
    })->name('admin-registration');

    Route::get('/admin/dashboard', function () {
        return view('admin::dashboard');
    })->name('admin-dashboard');*/

    Route::get('vendor/dashboard', 'Vendorism\Vendor\Http\Controllers\VendorsController@dashboard')->name('vendor-dashboard');


    Route::get('admin/vendors', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendors')->middleware('admin_loggedin')->name('admin-vendors');

    Route::get('admin/vendor/add', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendor_add')->middleware('admin_loggedin')->name('admin-vendor-add');
    Route::post('admin/vendor/store', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendor_store')->middleware('admin_loggedin')->name('admin-vendor-store');

    Route::get('admin/vendor/edit/{vendor}', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendor_edit')->middleware('admin_loggedin')->name('admin-vendor-edit');
    Route::put('admin/vendor/update/{vendor}', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendor_update')->middleware('admin_loggedin')->name('admin-vendor-update');

    Route::post('admin/vendor/delete/{vendor}', 'Vendorism\Vendor\Http\Controllers\VendorsController@admin_vendor_delete')->middleware('admin_loggedin')->name('admin-vendor-delete');



});


