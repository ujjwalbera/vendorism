@extends('layouts.admin.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Vendor listing</h3>
                <span class="float-right">
                    <a href="<?php echo route('admin-vendor-add'); ?>" class="btn btn-block btn-outline-primary btn-sm">Add New</a>
                </span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vendors as $vendor)
                    <tr>
                    <td>@if(isset($vendor->profile->name)) {{ $vendor->profile->name }}@endif</td>
                        <td>@if(isset($vendor->profile->email)) {{ $vendor->profile->email }}@endif</td>
                        <td>@if(isset($vendor->profile->phone)) {{ $vendor->profile->phone }}@endif</td>
                        <td>@if(isset($vendor->profile->address)) {{ $vendor->profile->address }}@endif</td>
                        <td>
                            @if($vendor->status == true)
                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active"
                                     title="Active" class="statusbutton">
                            @else
                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                     title="In-Active" class="statusbutton">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin-vendor-edit', ['vendor' => $vendor->id]) }}">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            <span class="button-divider">|</span>
                            <form action="{{ route('admin-vendor-delete', ['vendor' => $vendor->id]) }}" method="POST" class="delete-form">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="delete-form-button">
                                    <i class="fas fa-trash"></i> Delete
                                </button >
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
