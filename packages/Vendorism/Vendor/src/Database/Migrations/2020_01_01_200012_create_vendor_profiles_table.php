<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id')->unsigned();
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('phone', 10)->unique();
            $table->string('slug', 15)->unique();
            $table->string('store_name', 50)->nullable();
            $table->string('address')->nullable();
            $table->string('latitude', 10)->unique();
            $table->string('longitude', 10)->unique();
            $table->string('city', 50)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 15)->nullable();
            $table->string('store_no', 15)->nullable();
            $table->time('open_time')->nullable();
            $table->time('close_time')->nullable();
            $table->string('close_day', 15)->nullable();
            $table->string('gst_no', 15)->unique();
            $table->boolean('confirmed');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes(0);
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->index(['address', 'city', 'state', 'country']);
            $table->index(['close_day', 'confirmed', 'status']);
            $table->index(['open_time', 'close_time']);
            $table->index(['latitude', 'longitude']);
        });
        DB::update("ALTER TABLE vendor_profiles AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_profiles');
    }
}
