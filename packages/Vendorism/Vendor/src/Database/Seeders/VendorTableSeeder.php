<?php

namespace Vendorism\Vendor\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;

class VendorTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('vendors')->delete();

        DB::table('vendors')->insert([
            [
                'id' => 1,
                'email' => 'ujjwal.test@gmail.com',
                'email_verified_at' => '2020-01-06 07:32:44',
                'email_verify_token' => true,
                'username' => 'ujjwalbera',
                'password' => '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC',
                'role_id' => 1,
                'verified' => true,
                'status' => true,
                'remember_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'created_at' => '2020-01-06 07:29:44',
                'updated_at' => '2020-01-06 07:29:44'
            ]
        ]);
    }
}
