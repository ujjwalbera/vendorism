<?php

namespace Vendorism\Vendor\Seeds;

use Illuminate\Database\Seeder;
use DB;

class VendorProfileTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('vendor_profiles')->delete();

        DB::table('vendor_profiles')->insert([
            [
                'id' => 1,
                'vendor_id' => 1,
                'name' => 'Ujjwal Bera',
                'email' => 'ujjwal.test@gmail.com',
                'phone' => '8981987974',
                'slug' => 'usha-digital',
                'store_name' => 'Usha Digital Seva Kendra',
                'address' => 'Bonhijli, Khanakul, Hooghly',
                'latitude' => '22.634200',
                'longitude' => '87.842914',
                'city' => 'Hooghly',
                'state' => 'West Bengal',
                'country' => 'India',
                'store_no' => '001',
                'open_time' => '08:00',
                'close_time' => '20:30',
                'close_day' => 'Sunday',
                'gst_no' => '07AAACB5343E1Z3',
                'confirmed' => true,
                'status' => true,
                'created_at' => '2020-01-06 07:29:44',
                'updated_at' => '2020-01-06 07:29:44'
            ]

        ]);
    }
}
