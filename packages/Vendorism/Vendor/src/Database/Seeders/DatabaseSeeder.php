<?php

namespace Vendorism\Vendor\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VendorTableSeeder::class);
        $this->call(VendorProfileTableSeeder::class);
    }
}
