<?php

namespace Vendorism\Product\Providers;

use Illuminate\Support\ServiceProvider;


class ProductServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/views/product', 'product');
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/category'),
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/role'),
        ]);*/
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //$this->registerConfig();
    }

    public function registerConfig() {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/product-types.php', 'product-types'
        );
    }

    public function composeView() {
        view()->composer(['admin::catalog.products.create'], function ($view) {
            $items = array();

            foreach (config('product-types') as $item) {
                $item['children'] = [];

                array_push($items, $item);
            }

            $types = core()->sortItems($items);

            $view->with('productTypes', $types);
        });
    }
}
