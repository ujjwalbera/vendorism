@if($attribute->type == 'text')
    <input type="text" name="{{ $attribute->code }}" @if($attribute->is_required == true) required @endif
    class="form-control" placeholder="Enter {{ $attribute->admin_name }}">
@elseif($attribute->type == 'textarea')
    <textarea class="form-control" name="{{ $attribute->code }}" @if($attribute->is_required == true) required
              @endif rows="3" placeholder="Enter {{ $attribute->admin_name }}"></textarea>
@elseif($attribute->type == 'select')
    <select name="{{ $attribute->code }}" id="{{ $attribute->code }}" @if($attribute->is_required == true) required
            @endif class="form-control select2">
        @if(count($attribute->options))
            @foreach($attribute->options as $option)
                <option value="{{ $option->id }}">{{ $option->admin_name }}</option>
            @endforeach
        @endif
    </select>
@elseif($attribute->type == 'date')
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
        </div>
        <input type="text" name="{{ $attribute->code }}" @if($attribute->is_required == true) required @endif class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
    </div>
@elseif($attribute->type == 'boolean')
    <div class="form-group clearfix">
        <div class="icheck-success d-inline">
            <input type="radio" name="{{ $attribute->code }}" @if($attribute->is_required == true) required @endif checked="" id="radioSuccess{{ $attribute->id }}">
            <label for="radioSuccess{{ $attribute->id }}" style="font-size: 12px; font-weight: 500;">Active
            </label>
        </div>
        <div class="icheck-danger d-inline">
            <input type="radio" name="{{ $attribute->code }}" @if($attribute->is_required == true) required @endif id="radioDanger{{ $attribute->id }}">
            <label for="radioDanger{{ $attribute->id }}" style="font-size: 12px; font-weight: 500;">In-Active
            </label>
        </div>
    </div>
@elseif($attribute->type == 'price')
    <div class="input-group">
        <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="fas fa-dollar-sign"></i>
                    </span>
        </div>
        <input type="number" name="{{ $attribute->code }}" @if($attribute->is_required == true) required @endif class="form-control" placeholder="Enter {{
        $attribute->admin_name }}">
        <div class="input-group-append">
            <div class="input-group-text"><i class="fas fa-shopping-cart"></i></div>
        </div>
    </div>
@else
    {{ $attribute->type }}
@endif
