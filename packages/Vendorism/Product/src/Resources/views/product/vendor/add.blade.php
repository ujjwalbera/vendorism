@extends('layouts.vendor.main')
@section('content')
<!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title" style="float: left;">Product Add</h3>
              <span class="float-right">
                <a href="<?php echo route('vendor-products'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
              <div class="flash-message">
                  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                      @if(Session::has('alert-' . $msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endif
                  @endforeach
              </div>
          </div>
            @if (Session::has('message'))
                <div class="alert alert-warning">{{ Session::get('message') }}</div>
            @endif
            <form role="form" action="{{ route('vendor-product-store') }}" method="post">
                <!-- /.card-header -->
                @csrf
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Choose parent Product</label>
                    <select name="parent" id="parent"  class="form-control select2">
                        <option value="null">No Parent</option>
                        @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>
                    @error('parent') <div class="error"> {{ $message }} </div> @enderror
                </div>
                <!-- /.form-group -->
                  <div class="form-group">
                      <label>Choose Product Family</label>
                      <select name="family" id="family"  class="form-control select2">
                          @foreach($families as $family)
                              <option value="{{ $family->id }}">{{ $family->name }}</option>
                          @endforeach
                      </select>
                      @error('parent') <div class="error"> {{ $message }} </div> @enderror
                  </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Product Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Product name">
                    @error('name') <div class="error"> {{ $message }} </div> @enderror
                </div>
                <!-- /.form-group -->

              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
              <div class="card-footer">
                  <button type="submit" class="btn btn-success">Process <i class="fas fa-arrow-circle-right"></i></button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
              </div>
          </div>
            </form>
          <!-- /.card-body -->
          <div class="card-footer">
            <!--Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information
              about
            the plugin.-->
          </div>

        </div>
        <!-- /.card -->
@endsection
