@extends('layouts.admin.main')
@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Product Edit: {{ $current->name }}</h3>
            <span class="float-right">
                <a href="<?php echo route('admin-products'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        @if (Session::has('message'))
            <div class="alert alert-warning">{{ Session::get('message') }}</div>
        @endif
        <form role="form" action="{{ route('admin-product-update', ['family' => $current->attribute_family->code,
                            'product' => $current->id]) }}" method="post">
            <!-- /.card-header -->
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 card-header" style="margin-bottom: 20px;"><h3 class="card-title">Product
                            Common</h3></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Choosen parent Product</label>
                            <select name="parent" id="parent"  class="form-control select2">
                                <option value="null">No Parent</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}" @if($product->parent_id == $current->id) selected @endif >{{ $product->name }}</option>
                                @endforeach
                            </select>
                            @error('parent') <div class="error"> {{ $message }} </div> @enderror
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Choose Product Vendor</label>
                            <select name="vendor" id="vendor"  class="form-control select2">
                                @foreach($vendors as $vendor)
                                    <option value="{{ $vendor->id }}" @if($vendor->status == false) disable @endif @if($vendor->id == $current->vendor_id) selected
                                        @endif>{{
                            $vendor->username }}</option>
                                @endforeach
                            </select>
                            @error('vendor') <div class="error"> {{ $message }} </div> @enderror
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Product Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Product name" value="{{ $current->name }}">
                            @error('name') <div class="error"> {{ $message }} </div> @enderror
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Choose Product Family</label>
                            <select name="family" id="family"  class="form-control select2">
                                @foreach($families as $family)
                                    <option value="{{ $family->id }}" @if($family->id != $current->attribute_family_id) disabled @endif @if($family->id == $current->attribute_family_id) selected @endif >{{ $family->name }}</option>
                                @endforeach
                            </select>
                            @error('parent') <div class="error"> {{ $message }} </div> @enderror
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                @foreach($attributeGroups as $attributeGroup)
                    <div class="row">
                        <div class="col-md-12 card-header" style="margin-bottom: 20px;"><h3 class="card-title">{{ $attributeGroup->name }}</h3></div>
                        @if(count($attributeGroup->group_attributes))
                            @foreach($attributeGroup->group_attributes as $attribute)
                                @if($attribute->status)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-size: 13px; font-weight: 500;">{{ $attribute->admin_name }}</label>
                                            @include('product::admin.attribute_group_fields', ['attribute' => $attribute, 'current'
             => $current, 'categories' =>  $categories])
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                @endif
                            @endforeach
                        @endif

                        <!-- /.col -->
                    </div>
                @endforeach
                <!-- /.row -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
                    <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
            </div>



        </form>
        <!-- /.card-body -->
        <div class="card-footer">
            Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information
              about
            the plugin.
        </div>

    </div>
    <!-- /.card -->
@endsection
