@extends('layouts.admin.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Product Category listing</h3>
                <span class="float-right">
                    <a href="<?php echo route('admin-product-add'); ?>" class="btn btn-block btn-outline-primary btn-sm">Add New</a>
                </span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Attribute Family</th>
                        <th>User</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>@if(isset($product->name)) {{ $product->name }}@endif</td>
                        <td>@if(isset($product->parent->name)) {{ $product->parent->name }}@endif</td>
                        <td>@if(isset($product->attribute_family->name)) {{ $product->attribute_family->name }}@endif</td>
                        <td>@if(isset($product->vendor->username)) {{ $product->vendor->username }}@endif</td>
                        <td><!--@if(isset($product->name)) {{ $product->name }}@endif--></td>
                        <td>
                            @if($product->status == true)
                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active"
                                     title="Active" class="statusbutton">
                            @else
                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                     title="In-Active" class="statusbutton">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin-product-edit', ['family' => $product->attribute_family->code,
                            'product' => $product->id]) }}">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            /
                            <a href="{{ route('admin-product-delete', ['product' => $product->id]) }}" style="color:#ff0000;">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Attribute Family</th>
                        <th>User</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row (main row) -->
@endsection
