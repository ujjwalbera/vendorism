@if($attribute->type == 'text')
    <input type="text" name="{{ $attribute->code }}" class="form-control" placeholder="Enter {{ $attribute->admin_name }}" value="{{ old($attribute->code)}}" @if($attribute->is_required == true) required @endif >
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'textarea')
    <textarea class="form-control" name="{{ $attribute->code }}" rows="3" placeholder="Enter {{ $attribute->admin_name }}" value="{{ old($attribute->code)}}" @if($attribute->is_required == true) required @endif></textarea>
              @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'select')
    <select name="{{ $attribute->code }}" id="{{ $attribute->code }}" class="form-control select2" @if($attribute->is_required == true) required @endif>
        @if(count($attribute->options))
            @foreach($attribute->options as $option)
                <option value="{{ $option->id }}" @if(old('{{$attribute->code}}') == $option->id) selected @endif>{{ $option->frontend_name }}</option>
            @endforeach
        @endif
    </select>
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'date')
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
        </div>
        <input type="text" name="{{ $attribute->code }}" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask  value="{{ old($attribute->code)}}" @if($attribute->is_required == true) required @endif>
    </div>
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'boolean')
    <div class="form-group clearfix">
        <div class="icheck-success d-inline">
            <input type="radio" name="{{ $attribute->code }}" checked="" id="radioSuccess{{ $attribute->id }}">
            <label for="radioSuccess{{ $attribute->id }}" style="font-size: 12px; font-weight: 500;">Active
            </label>
        </div>
        <div class="icheck-danger d-inline">
            <input type="radio" name="{{ $attribute->code }}" id="radioDanger{{ $attribute->id }}">
            <label for="radioDanger{{ $attribute->id }}" style="font-size: 12px; font-weight: 500;">In-Active
            </label>
        </div>
    </div>
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'price')
    <div class="input-group">
        <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="fas fa-dollar-sign"></i>
                    </span>
        </div>
        <input type="number" name="{{ $attribute->code }}" class="form-control" placeholder="Enter {{ $attribute->admin_name }}" value="{{ old($attribute->code)}}" @if($attribute->is_required == true) required @endif>
        <div class="input-group-append">
            <div class="input-group-text"><i class="fas fa-shopping-cart"></i></div>
        </div>
    </div>
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'category')
    <select name="{{ $attribute->code }}" id="{{ $attribute->code }}" class="form-control select2" @if($attribute->is_required == true) required @endif>
        @foreach($categories as $category)
            <option value="{{ $category->id }}" @if(old('{{$attribute->code}}') == $category->id) selected @endif>{{ $category->name }}</option>
        @endforeach
    </select>
    @error('{{ $attribute->code }}') <div class="error"> {{ $message }} </div> @enderror
@elseif($attribute->type == 'image')
    <div class="custom-file">
        <input type="file" class="custom-file-input" name="{{ $attribute->code }} id="{{ $attribute->code }}">
        <label class="custom-file-label" for="customFile">Choose {{ $attribute->admin_name }}</label>
    </div>
@elseif($attribute->type == 'images')
    <div class="custom-file">
        <input type="file" multiple="true" class="custom-file-input" name="{{ $attribute->code }} id="{{ $attribute->code }}">
        <label class="custom-file-label" for="customFile">Choose {{ $attribute->admin_name }}</label>
    </div>
@elseif($attribute->type == 'file')
    <div class="custom-file">
        <input type="file" class="custom-file-input" name="{{ $attribute->code }} id="{{ $attribute->code }}">
        <label class="custom-file-label" for="customFile">Choose {{ $attribute->admin_name }}</label>
    </div>
@elseif($attribute->type == 'files')
    <div class="custom-file">
        <input type="file" multiple="true" class="custom-file-input" name="{{ $attribute->code }} id="{{ $attribute->code }}">
        <label class="custom-file-label" for="customFile">Choose {{ $attribute->admin_name }}</label>
    </div>
@else
    {{ $attribute->type }}
@endif
