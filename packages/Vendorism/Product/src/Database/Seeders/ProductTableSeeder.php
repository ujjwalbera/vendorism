<?php

namespace Vendorism\Product\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use DB;

class ProductTableSeeder extends Seeder
{
    public function run()
    {
        //dd('f607d2a7-a744-40ec-b4be-1250a270224b');

        DB::table('products')->delete();
        $now = Carbon::now();
        DB::table('products')->insert([
            [
                'id' => 1,
                'name' => 'Masoor Dal',
                'vendor_id' => 1,
                'parent_id' => 0,
                'attribute_family_id' => 1,
                'deleted_at' => NULL,
                'status' => true,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);

        DB::table('products')->insert([
            [
                'id' => 2,
                'name' => 'Moong Dal',
                'vendor_id' => 1,
                'parent_id' => 0,
                'attribute_family_id' => 1,
                'deleted_at' => NULL,
                'status' => true,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);

        DB::table('products')->insert([
            [
                'id' => 3,
                'name' => 'Toor/Arhar Dal',
                'vendor_id' => 1,
                'parent_id' => 0,
                'attribute_family_id' => 1,
                'deleted_at' => NULL,
                'status' => true,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);

        DB::table('products')->insert([
            [
                'id' => 4,
                'name' => 'Peas/Matar - White',
                'vendor_id' => 1,
                'parent_id' => 0,
                'attribute_family_id' => 1,
                'deleted_at' => NULL,
                'status' => true,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
