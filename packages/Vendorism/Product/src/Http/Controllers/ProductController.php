<?php

namespace Vendorism\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Vendorism\Product\Models\Product;
use Vendorism\Vendor\Models\Vendor;
use Vendorism\Attribute\Models\AttributeFamily;
use Vendorism\Attribute\Models\AttributeGroup;
use Vendorism\Attribute\Models\AttributeOption;
use Vendorism\Attribute\Models\Attribute;
use Vendorism\Category\Models\Category;
use Vendorism\Product\Models\ProductAttributeValue;




/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductController extends Controller
{
    /**
     * AttributeFamilyRepository object
     *
     * @var array
     */
    protected $attributeFamily;

    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $category;

    /**
     * InventorySourceRepository object
     *
     * @var array
     */
    protected $inventorySource;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * ProductAttributeValueRepository object
     *
     * @var array
     */
    protected $productAttributeValue;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        $products = Product::where('deleted_at', null)->get();
        //$query = DB::getQueryLog();
        //dd($query);
        //dd($products->toArray());
        return view('product::admin.products', compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        $vendor_id = Auth::guard('vendor')->user()->id;
        $products = Product::where('vendor_id', $vendor_id)->where('deleted_at', null)->get();
        //$query = DB::getQueryLog();
        //dd($query);
        //dd($products->toArray());
        return view('product::vendor.products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_create()
    {
        $families = AttributeFamily::where('deleted_at', NULL)->get();
        $vendors = Vendor::where('deleted_at', NULL)->get();
        $products = Product::where('deleted_at', NULL)->where('parent_id', 0)->get();
        return view('product::admin.add', compact('families', 'vendors', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_create()
    {
        $families = AttributeFamily::where('deleted_at', null)->get();
        $products = Product::where('deleted_at', null)->where('parent_id', null)->get();
        return view('product::vendor.add', compact('families', 'products'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_store(Request $request)
    {
        Event::dispatch('product.create.before');
        $data = $request->validate([
            'parent' => 'required',
            'vendor' => 'required',
            'family' => 'required',
            'name' => 'required|regex:/^[A-Za-z0-9 ]+$/',
        ]);
        $parent_id = $request->input( 'parent' );
        if($request->input( 'parent' ) == "null") {
            $parent_id = null;
        }
        $product = new Product();
        $product->name = $request->input( 'name' );
        $product->status = true;
        $product->parent_id = $parent_id;
        $product->vendor_id = $request->input( 'vendor' );
        $product->attribute_family_id = $request->input( 'family' );
        $product->save();

        return redirect()->route('admin-product-edit', ['family' => $product->attribute_family->code, 'product' =>
            $product->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_store(Request $request)
    {
        Event::dispatch('product.create.before');
        $data = $request->validate([
            'parent' => 'required',
            'family' => 'required',
            'name' => 'required|regex:/^[A-Za-z0-9 ]+$/',
        ]);
        $parent_id = $request->input( 'parent' );
        if($request->input( 'parent' ) == "null") {
            $parent_id = null;
        }
        $product = new Product();
        $product->name = $request->input( 'name' );
        $product->status = true;
        $product->parent_id = $parent_id;
        $product->vendor_id = Auth::guard('vendor')->user()->id;
        $product->attribute_family_id = $request->input( 'family' );
        $product->save();

        return redirect()->route('vendor-product-edit', ['family' => $product->attribute_family->code, 'product' =>
            $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_edit($family, $product)
    {
        $products = Product::where('deleted_at', null)->where('parent_id', null)->get();
        $current = Product::where('deleted_at', null)->where('id', $product)->first();
        if(count($current->attribute_values)) {
            $productAttr = $current->attribute_values;
            //print_r($productAttr->toArray()); die;
        }
        $attributeFamily = AttributeFamily::where('deleted_at', null)->where('code', $family)->first();
        //$categories = Category::where('deleted_at', null)->get();
        $families = AttributeFamily::where('deleted_at', null)->get();
        $categories = Category::where('status', true)->get();
        $vendors = Vendor::where('deleted_at', null)->get();
        $categories = Category::all();
        $attributeGroups = AttributeGroup::where('deleted_at', null)->where('attribute_family_id',
                $attributeFamily->id)->orderBy('position')->get();
        //dd($categories->toArray());
        return view('product::admin.edit', compact('products', 'current', 'categories', 'attributeGroups', 'families', 'vendors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Product\Http\Requests\ProductForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_update(Request $request, $family_code, $product_id)
    {
        $product = Product::where('id', $product_id)->first();
        $attributeFamily = AttributeFamily::where('deleted_at', null)->where('code', $family_code)->first();
        $attributeGroups = AttributeGroup::where('deleted_at', null)->where('attribute_family_id', $attributeFamily->id)->orderBy('position')->get();
        Event::dispatch('product.update.before');
        $data = $request->validate([
            'parent' => 'required',
            'family' => 'required'
        ]);
        $parent_id = $request->input( 'parent' );
        if($request->input( 'parent' ) == "null") {
            $parent_id = null;
        }
        $product->name = $request->input( 'name' );
        $product->status = true;
        $product->parent_id = $parent_id;
        $product->vendor_id = $request->input( 'vendor' );
        $product->attribute_family_id = $request->input( 'family' );
        $product->save();
        $productattributes = [];
        $productattribute = [];
        foreach($attributeGroups as $attributeGroup) {
            if(count($attributeGroup->group_attributes)) {
                foreach($attributeGroup->group_attributes as $attribute) {
                    $rule = '';
                    if($attribute->status) {

                        if($attribute->is_required) {
                            $rule.= 'required';
                        }
                        if($attribute->is_unique) {
                            //$rule.= '|unique:product_attribute_values,'.$attribute->code.','.$attribute->id.',attribute_id';
                        }
                        $validator = $request->validate([
                            $attribute->code => $rule
                        ]);

                    }

                    $productattribute = [
                        'product_id' => $product->id,
                        'attribute_id' => $attribute->id,
                        'vendor_id' => $product->vendor_id,
                        'attribute_value' => $request->input( $attribute->code ),
                        'attribute_type' => $attribute->type
                    ];
                    $productattributes[] = $productattribute;
                }
            }
        }

        foreach($productattributes as $productattribute){
            $ProductAttributeValue = ProductAttributeValue::where('product_id', $productattribute['product_id'])->where('vendor_id', $productattribute['vendor_id'])->where('attribute_id', $productattribute['attribute_id'])->first();
            if(!$ProductAttributeValue) {
                $ProductAttributeValue = new ProductAttributeValue();
            }

            $ProductAttributeValue->id = Str::uuid();
            $ProductAttributeValue->product_id = $productattribute['product_id'];
            $ProductAttributeValue->attribute_id = $productattribute['attribute_id'];
            $ProductAttributeValue->vendor_id = $productattribute['vendor_id'];
            if($productattribute['attribute_type'] == 'text'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'select'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'multi-select'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'boolean'){
                $ProductAttributeValue->boolean_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'radio'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'checkbox'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'textarea'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'price'){
                $ProductAttributeValue->float_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'date'){
                $ProductAttributeValue->date_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'number'){
                $ProductAttributeValue->integer_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'email'){
                $ProductAttributeValue->text_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'datetime'){
                $ProductAttributeValue->datetime_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'file'){
                $ProductAttributeValue->datetime_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'files'){
                $ProductAttributeValue->datetime_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'image'){
                $ProductAttributeValue->datetime_value = $productattribute['attribute_value'];
            } else if($productattribute['attribute_type'] == 'images'){
                $ProductAttributeValue->datetime_value = $productattribute['attribute_value'];    
            }
            $ProductAttributeValue->save();
        }

        return redirect()->route('admin-products');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function vendor_edit($family, $product)
    {
        $vendor_id = Auth::guard('vendor')->user()->id;
        $products = Product::where('deleted_at', null)->where('parent_id', null)->where('vendor_id', $vendor_id)->get();
        $current = Product::where('deleted_at', null)->where('id', $product)->first();
        if(count($current->attribute_values)) {
            $productAttr = $current->attribute_values;
            //print_r($productAttr->toArray()); die;
        }
        $attributeFamily = AttributeFamily::where('deleted_at', null)->where('code', $family)->first();
        //$categories = Category::where('deleted_at', null)->get();
        $families = AttributeFamily::where('deleted_at', null)->get();
        $categories = Category::where('status', true)->get();
        $attributeGroups = AttributeGroup::where('deleted_at', null)->where('attribute_family_id', $attributeFamily->id)->orderBy('position')->get();
        //dd($attributeGroups->toArray());
        return view('product::vendor.edit', compact('products', 'current', 'categories', 'attributeGroups',
            'families'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Product\Http\Requests\ProductForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function vendor_update(Request $request, $family, $product)
    {
        Event::dispatch('product.update.before');
        $data = $request->validate([
            'parent' => 'required',
            'family' => 'required',
            'name' => 'required|regex:/^[A-Za-z0-9 ]+$/',
        ]);
        $parent_id = $request->input( 'parent' );
        if($request->input( 'parent' ) == "null") {
            $parent_id = null;
        }
        $vendor_id = Auth::guard('vendor')->user()->id;
        $product = Product::where('vendor_id', $vendor_id)->where('id', $product)->first();
        $product->name = $request->input( 'name' );
        $product->status = true;
        $product->parent_id = $parent_id;
        $product->attribute_family_id = $request->input( 'family' );
        $product->save();

        $attributes = $product->attribute_family->custom_attributes;

        dd(request()->all());

        $product = $this->product->update(request()->all(), $product);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Product']));

        return redirect()->route('vendor-products');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $families = $this->attributeFamily->all();

        $configurableFamily = null;

        if ($familyId = request()->get('family')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        return view($this->_config['view'], compact('families', 'configurableFamily'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     **/
    public function store()
    {
        if (! request()->get('family') && request()->input('type') == 'configurable' && request()->input('sku') != '') {
            return redirect(url()->current() . '?family=' . request()->input('attribute_family_id') . '&sku=' . request()->input('sku'));
        }

        if (request()->input('type') == 'configurable' && (! request()->has('super_attributes') || ! count(request()->get('super_attributes')))) {
            session()->flash('error', trans('admin::app.catalog.products.configurable-error'));

            return back();
        }

        $this->validate(request(), [
            'type' => 'required',
            'attribute_family_id' => 'required',
            'sku' => ['required', 'unique:products,sku']
        ]);

        $product = $this->product->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Product']));

        return redirect()->route($this->_config['redirect'], ['id' => $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->product->with(['variants', 'variants.inventories'])->findOrFail($id);

        $categories = $this->category->getCategoryTree();

        $inventorySources = $this->inventorySource->all();

        return view($this->_config['view'], compact('product', 'categories', 'inventorySources'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Product\Http\Requests\ProductForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = $this->product->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Product']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);

        try {
            $this->product->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Product']));

            return response()->json(['message' => true], 200);
        } catch (\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Product']));
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Mass Delete the products
     *
     * @return response
     */
    public function massDestroy()
    {
        $productIds = explode(',', request()->input('indexes'));

        foreach ($productIds as $productId) {
            $product = $this->product->find($productId);

            if (isset($product)) {
                $this->product->delete($productId);
            }
        }

        session()->flash('success', trans('admin::app.catalog.products.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Mass updates the products
     *
     * @return response
     */
    public function massUpdate()
    {
        $data = request()->all();

        if (!isset($data['massaction-type'])) {
            return redirect()->back();
        }

        if (!$data['massaction-type'] == 'update') {
            return redirect()->back();
        }

        $productIds = explode(',', $data['indexes']);

        foreach ($productIds as $productId) {
            $this->product->update([
                'channel' => null,
                'locale' => null,
                'status' => $data['update-options']
            ], $productId);
        }

        session()->flash('success', trans('admin::app.catalog.products.mass-update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /*
     * To be manually invoked when data is seeded into products
     */
    public function sync()
    {
        Event::fire('products.datagrid.sync', true);

        return redirect()->route('admin.catalog.products.index');
    }

    /**
     * Result of search product.
     *
     * @return \Illuminate\Http\Response
     */
    public function productLinkSearch()
    {
        if (request()->ajax()) {
            $results = [];

            foreach ($this->product->searchProductByAttribute(request()->input('query')) as $row) {
                $results[] = [
                        'id' => $row->product_id,
                        'sku' => $row->sku,
                        'name' => $row->name,
                    ];
            }

            return response()->json($results);
        } else {
            return view($this->_config['view']);
        }
    }

     /**
     * Download image or file
     *
     * @param  int $productId, $attributeId
     * @return \Illuminate\Http\Response
     */
    public function download($productId, $attributeId)
    {
        $productAttribute = $this->productAttributeValue->findOneWhere([
            'product_id'   => $productId,
            'attribute_id' => $attributeId
        ]);

        return Storage::download($productAttribute['text_value']);
    }
}
