<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {

    Route::get('admin/products', 'Vendorism\Product\Http\Controllers\ProductController@admin_list')->middleware('admin_loggedin')->name('admin-products');
    Route::get('admin/product/add', 'Vendorism\Product\Http\Controllers\ProductController@admin_create')->middleware('admin_loggedin')->name('admin-product-add');
    Route::post('admin/product/store/', 'Vendorism\Product\Http\Controllers\ProductController@admin_store')->middleware('admin_loggedin')->name('admin-product-store');
    Route::get('admin/product/edit/{family}/{product}', 'Vendorism\Product\Http\Controllers\ProductController@admin_edit')->middleware('admin_loggedin')->name('admin-product-edit');
    Route::post('admin/product/update/{family}/{product}', 'Vendorism\Product\Http\Controllers\ProductController@admin_update')->middleware('admin_loggedin')->name('admin-product-update');
    Route::get('admin/product/delete', 'Vendorism\Product\Http\Controllers\ProductController@admin_destroy')->middleware('admin_loggedin')->name('admin-product-delete');

    Route::get('vendor/products', 'Vendorism\Product\Http\Controllers\ProductController@vendor_list')->middleware('vendor_loggedin')->name('vendor-products');
    Route::get('vendor/product/add', 'Vendorism\Product\Http\Controllers\ProductController@vendor_create')->middleware('vendor_loggedin')->name('vendor-product-add');
    Route::post('vendor/product/store/', 'Vendorism\Product\Http\Controllers\ProductController@vendor_store')->middleware('vendor_loggedin')->name('vendor-product-store');
    Route::get('vendor/product/edit/{family}/{product}', 'Vendorism\Product\Http\Controllers\ProductController@vendor_edit')->middleware('vendor_loggedin')->name('vendor-product-edit');
    Route::post('vendor/product/update/{family}/{product}', 'Vendorism\Product\Http\Controllers\ProductController@vendor_update')->middleware('vendor_loggedin')->name('vendor-product-update');
    Route::get('vendor/product/delete', 'Vendorism\Product\Http\Controllers\ProductController@vendor_destroy')->middleware('vendor_loggedin')->name('vendor-product-delete');

});


