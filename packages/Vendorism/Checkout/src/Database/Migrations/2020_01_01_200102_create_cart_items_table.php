<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('cart_id')->unsigned();
            $table->bigInteger('tax_category_id')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->default(0);
            $table->string('coupon_code')->nullable();
            $table->decimal('weight', 12,4)->default(1);
            $table->decimal('total_weight', 12,4)->default(0);
            $table->decimal('price', 12,4)->default(1);
            $table->decimal('base_price', 12,4)->default(0);
            $table->decimal('custom_price', 12,4)->default(0);
            $table->decimal('total', 12,4)->default(0);
            $table->decimal('tax_percent', 12, 4)->default(0)->nullable();
            $table->decimal('tax_amount', 12, 4)->default(0)->nullable();
            $table->decimal('discount_percent', 12,4)->default(0);
            $table->decimal('discount_amount', 12,4)->default(0);
            $table->json('additional')->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('cart_id')->references('id')->on('cart')->onDelete('cascade');
            #$table->foreign('tax_category_id')->references('id')->on('tax_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}
