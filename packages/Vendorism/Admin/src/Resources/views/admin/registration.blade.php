@extends('layouts.admin.login')
@section('content')
<div class="card">
    <div class="card-body register-card-body">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>
        <p class="login-box-msg">Register a new membership</p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{action('\Vendorism\Admin\Http\Controllers\AdminsController@doRegistration')}}" method="post">
            @error('name') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="name" type="text" class="form-control" placeholder="Full name" value="{{ old('name')}}" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>

            </div>
            @error('email') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email')}}" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            @error('username') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="username" type="text" class="form-control" placeholder="Username" value="{{ old
                ('username')}}" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            @error('password') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="password" type="password" class="form-control" placeholder="Password" value="" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            @error('password_confirmation') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="password_confirmation" type="password" class="form-control" placeholder="Retype password"
                       value="" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    @error('terms') <div class="error"> {{ $message }} </div> @enderror
                    <div class="icheck-primary">
                        <input type="checkbox" id="agreeTerms" name="terms" value="1">
                        <label for="agreeTerms">
                            I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
            @csrf
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="<?php echo route('admin-login'); ?>" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
</div>
@endsection
