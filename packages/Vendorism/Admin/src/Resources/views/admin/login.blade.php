@extends('layouts.admin.login')
@section('content')
<div class="card">
    <div class="card-body login-card-body">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>
        <p class="login-box-msg">Sign in to start your session</p>

        @if (Session::has('message'))
            <div class="alert alert-warning">{{ Session::get('message') }}</div>
        @endif
        <form action="{{action('\Vendorism\Admin\Http\Controllers\AdminsController@doLogin')}}" method="post">
            @error('username') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="username" type="text" class="form-control" placeholder="Username" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            @error('password') <div class="error"> {{ $message }} </div> @enderror
            <div class="input-group mb-3">
                <input name="password" type="password" class="form-control" placeholder="Password" autocomplete="false">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember" name="remember">
                        <label for="remember">
                            Remember Me
                        </label>
                    </div>
                </div>
            @csrf
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <p class="mb-1">
            <a href="<?php echo route('admin-forgot'); ?>">I forgot my password</a>
        </p>
        <p class="mb-0">
            <a href="<?php echo route('admin-registration'); ?>" class="text-center">Register a new membership</a>
        </p>
    </div>
    <!-- /.login-card-body -->
</div>
@endsection
