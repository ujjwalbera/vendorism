@extends('layouts.admin.login')
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{action('\Vendorism\Admin\Http\Controllers\AdminsController@forgot')}}" method="post">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <p class="mb-1">
                <a href="<?php echo route('admin-login'); ?>" class="text-center">Try Login</a>
            </p>
            <p class="mb-0">
                <a href="<?php echo route('admin-registration'); ?>" class="text-center">Don't have membership?</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
@endsection
