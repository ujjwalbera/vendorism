<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () {

    Route::get('admin/registration', '\Vendorism\Admin\Http\Controllers\AdminsController@registration')->name('admin-registration');
    Route::post('admin/registration', '\Vendorism\Admin\Http\Controllers\AdminsController@doRegistration')->name('admin-doRegistration');

    Route::get('admin/login', '\Vendorism\Admin\Http\Controllers\AdminsController@login')->name('admin-login');
    Route::get('admin', '\Vendorism\Admin\Http\Controllers\AdminsController@login')->name('admin-login');
    Route::post('admin/login', '\Vendorism\Admin\Http\Controllers\AdminsController@doLogin')->name('admin-doLogin');

    Route::get('admin/logout', '\Vendorism\Admin\Http\Controllers\AdminsController@doLogout')->name('admin-logout');

    /*Route::get('/admin/forgot', function () {
        return view('admin::forgot');
    })->name('admin-forgot');*/
    Route::get('admin/forgot', '\Vendorism\Admin\Http\Controllers\AdminsController@admin_forgot')->name('admin-forgot');
    Route::post('admin/forgot', 'Vendorism\Admin\Http\Controllers\AdminsController@forgot')->name('admin-forgot-set');

    /*Route::get('/admin/registration', function () {
        return view('admin::registration');
    })->name('admin-registration');

    Route::get('/admin/dashboard', function () {
        return view('admin::dashboard');
    })->name('admin-dashboard');*/

    Route::get('admin/dashboard', 'Vendorism\Admin\Http\Controllers\AdminsController@dashboard')->name('admin-dashboard');



});


