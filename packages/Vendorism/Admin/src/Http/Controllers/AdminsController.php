<?php
namespace Vendorism\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
use Mail;

//use Vendorism\Admin\Repositories\RoleRepository as Role;

use phpDocumentor\Reflection\Types\Parent_;
use Vendorism\Admin\Models\Admin;
use Vendorism\Admin\Models\Profile;

class AdminsController extends Controller
{

    /**
     * RoleRepository object
     *
     * @var array
     */
    protected $role;


    protected $table = 'admins';


    protected $adminProfiles = 'admin_profiles';



    /**
     * Create a new controller instance.
     *
     * @param  \Vendorism\Admin\Repositories\RoleRepository  $role
     * @return void
     */
    public function __construct(/*Role $role*/)
    {
        Parent::__construct();
        $this->admins = 'admins';
        $this->adminProfiles = 'admin_profiles';
    }

    /**
     * @return void
     */
    public function login()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin-dashboard');
        }
        return view('admin::login');
    }



    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\User\Repositories\RoleRepository  $role
     * @return void
     */
    public function doLogin(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $data = $request->validate($rules);

        if (Auth::guard('admin')->attempt(array(
            'username' => $request->input('username'),
            'password' => $request->input('password')
        ), $request->input('remember'))) {

            $request->session()->flash('alert-success', 'User was successful login!');
            return redirect()->route('admin-dashboard');
        } else {
            $request->session()->flash('alert-warning', 'Invalid Credentials , Please try again.');
            return redirect()->route('admin-login');
        }
    }



    /**
     * @return void
     */
    public function forgot()
    {

    }



    /**
     * @return void
     */
    public function registration()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin-dashboard');
        }
        return view('admin::registration');
    }



    /**
     * @return void
     */
    public function doRegistration(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'email' => 'required|email|unique:' . $this->table,
            'username' => 'required|min:8|max:12|unique:' . $this->table,
            'password' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/',
            'password_confirmation' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/|same:password',
            'terms' => 'required'
        ];
        $data = $request->validate($rules);
        $admin = new Admin();
        $admin->email = $request->input( 'email' );
        $admin->username = $request->input( 'username' );
        $admin->password = Hash::make( $request->input( 'password' ) );
        $admin->role_id = 1;
        $admin->remember_token = $request->input( '_token' );
        $admin->save();
        $profile = new Profile();
        $profile->admin_id = $admin->id;
        $profile->name = $request->input( 'name' );
        $profile->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('alert-success', 'User was successful registration!');
        return redirect()->back();
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function registration_confirmation(Request $request, $id)
    {
        $user = Admin::findOrFail($id);

        Mail::send('admin::emails.confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from('ushadigitalsevakendra@gmail.com', 'Vendorism');

            $m->to($user->email, $user->name)->subject('Your registration confirmation!');
        });
    }

    /**
     * @return void
     */
    public function doLogout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flash('alert-success', 'User was successfully logged out!');
        return redirect()->route('admin-login');
    }



    /**
     * @return void
     */
    public function dashboard()
    {
        //$id = Auth::user()->id;
        //DB::connection()->enableQueryLog();
        //$admin = User::find($id)->admin()->get();
        //$queries = \DB::getQueryLog();
        return view('admin::dashboard');
    }


}
