<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->uuid('uuid')->primary();
            $table->string('email', 100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email_verify_token')->nullable();
            $table->string('username', 15)->unique();
            $table->string('password', 150);
            $table->integer('role_id');
            $table->boolean('verified')->default(false);
            $table->boolean('status')->default(false);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['email', 'username', 'role_id', 'status']);
        });
        DB::update("ALTER TABLE admins AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
