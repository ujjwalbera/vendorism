<?php

namespace Vendorism\Admin\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;

class AdminTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('admins')->delete();
        $now = Carbon::now();
        DB::table('admins')->insert([
                'id' => 1,
                'email' => 'ushadigitalsevakendra@gmail.com',
                'email_verified_at' => $now,
                'email_verify_token' => true,
                'username' => 'ushadigital',
                'password' => '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC',
                'role_id' => 1,
                'verified' => true,
                'status' => true,
                'remember_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'created_at' => $now,
                'updated_at' => $now
        ]);

    }
}
