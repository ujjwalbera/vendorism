<?php

namespace Vendorism\Admin\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminProfileTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admin_profiles')->delete();
        $now = Carbon::now();
        DB::table('admin_profiles')->insert([
            'id' => 1,
            'admin_id' => 1,
            'name' => 'Ujjwal Bera',
            'address' => 'Bonhijli, Khanakul, Hooghly',
            'status' => false,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
