<?php
namespace Vendorism\Carrier\Providers;

use Illuminate\Support\ServiceProvider;

class CarrierServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/views/carrier', 'carrier');
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/admin'),
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/role'),
        ]);*/
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
