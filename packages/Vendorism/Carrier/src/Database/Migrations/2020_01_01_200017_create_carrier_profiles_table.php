<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarrierProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('carrier_id')->unsigned();
            $table->string('slug', 15)->unique();
            $table->string('address')->nullable();
            $table->string('latitude', 10)->unique();
            $table->string('longitude', 10)->unique();
            $table->string('city', 50)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 15)->nullable();
            $table->boolean('confirmed')->default(false);
            $table->boolean('status')->default(false);
            $table->boolean('online')->default(false);
            $table->timestamps();
            $table->softDeletes(0);
            $table->foreign('carrier_id')->references('id')->on('carriers')->onDelete('cascade');
            $table->index(['address', 'city', 'state', 'country']);
            $table->index(['latitude', 'longitude', 'status']);
        });
        DB::update("ALTER TABLE carrier_profiles AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_profiles');
    }
}
