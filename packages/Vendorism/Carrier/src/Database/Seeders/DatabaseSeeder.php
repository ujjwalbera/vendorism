<?php

namespace Vendorism\Carrier\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CarrierTableSeeder::class);
        $this->call(CarrierProfileTableSeeder::class);
    }
}
