<?php

namespace Vendorism\Carrier\Seeds;

use Illuminate\Database\Seeder;
use DB;

class CarrierProfileTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('carrier_profiles')->delete();

        DB::table('carrier_profiles')->insert([
            [
                'id' => 1,
                'carrier_id' => 1,
                'slug' => 'usha-digital',
                'address' => 'Bonhijli, Khanakul, Hooghly',
                'latitude' => '22.634200',
                'longitude' => '87.842914',
                'city' => 'Hooghly',
                'state' => 'West Bengal',
                'country' => 'India',
                'status' => true,
                'online' => true,
                'created_at' => '2020-01-06 07:29:44',
                'updated_at' => '2020-01-06 07:29:44'
            ]

        ]);
    }
}
