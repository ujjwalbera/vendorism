<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () {

    Route::get('carrier/registration', '\Vendorism\Carrier\Http\Controllers\CarriersController@registration')->name('carrier-registration');
    Route::post('carrier/registration', '\Vendorism\Carrier\Http\Controllers\CarriersController@doRegistration')->name('carrier-doRegistration');

    Route::get('carrier/login', '\Vendorism\Carrier\Http\Controllers\CarriersController@login')->name('carrier-login');
    Route::get('carrier', '\Vendorism\Carrier\Http\Controllers\CarriersController@login')->name('carrier-login');
    Route::post('carrier/login', '\Vendorism\Carrier\Http\Controllers\CarriersController@doLogin')->name('carrier-doLogin');

    Route::get('carrier/logout', '\Vendorism\Carrier\Http\Controllers\CarriersController@doLogout')->name('carrier-logout');

    /*Route::get('carrier/forgot', function () {
        return view('carrier::forgot');
    })->name('carrier-forgot');*/

    Route::post('carrier/forgot', 'Vendorism\Carrier\Http\Controllers\CarriersController@doforgot')->name('carrier-doforgot')->name('carrier-forgot-set');

    /*Route::get('/admin/registration', function () {
        return view('admin::registration');
    })->name('admin-registration');

    Route::get('/admin/dashboard', function () {
        return view('admin::dashboard');
    })->name('admin-dashboard');*/

    Route::get('carrier/dashboard', 'Vendorism\Carrier\Http\Controllers\CarriersController@dashboard')->name('carrier-dashboard');


    Route::get('admin/carriers', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carriers')->middleware('admin_loggedin')->name('admin-carriers');

    Route::get('admin/carrier/add', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carrier_add')->middleware('admin_loggedin')->name('admin-carrier-add');
    Route::post('admin/carrier/store', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carrier_store')->middleware('admin_loggedin')->name('admin-carrier-store');

    Route::get('admin/carrier/edit/{carrier}', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carrier_edit')->middleware('admin_loggedin')->name('admin-carrier-edit');
    Route::put('admin/carrier/update/{carrier}', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carrier_update')->middleware('admin_loggedin')->name('admin-carrier-update');

    Route::post('admin/carrier/delete/{carrier}', 'Vendorism\Carrier\Http\Controllers\CarriersController@admin_carrier_delete')->middleware('admin_loggedin')->name('admin-carrier-delete');



});


