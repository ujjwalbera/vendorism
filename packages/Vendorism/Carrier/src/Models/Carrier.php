<?php

namespace Vendorism\Carrier\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Carrier extends Authenticatable
{
    use Notifiable,HasApiTokens;

    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    protected $table = 'carriers';

    protected $guard = 'carrier';

    protected $guarded = ['id', 'provider', 'provider_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email_verified_at', 'email_verify_token', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'id' => 'string'
    ];


    /**
     * Get the admin record associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('Vendorism\Carrier\Models\Profile');
    }



    public function carrier_vender()
    {
        return $this->belongsToMany('Vendorism\Vendor\Models\Vendor', 'vendor_carrier_mapping', 'vendor_id', 'carrier_id');
    }



}
