@extends('layouts.admin.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Carrier listing</h3>
                <span class="float-right">
                    <a href="<?php echo route('admin-carrier-add'); ?>" class="btn btn-block btn-outline-primary btn-sm">Add New</a>
                </span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($carriers as $carrier)
                    <tr>
                    <td>@if(isset($carrier->name)) {{ $carrier->name }}@endif</td>
                        <td>@if(isset($carrier->email)) {{ $carrier->email }}@endif</td>
                        <td>@if(isset($carrier->profile->phone)) {{ $carrier->profile->phone }}@endif</td>
                        <td>@if(isset($carrier->profile->address)) {{ $carrier->profile->address }}@endif</td>
                        <td>
                            @if($carrier->status == true)
                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active"
                                     title="Active" class="statusbutton">
                            @else
                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                     title="In-Active" class="statusbutton">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin-carrier-edit', ['carrier' => $carrier->id]) }}">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            <span class="button-divider">|</span>
                            <form action="{{ route('admin-carrier-delete', ['carrier' => $carrier->id]) }}" method="POST" class="delete-form">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="delete-form-button">
                                    <i class="fas fa-trash"></i> Delete
                                </button >
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
