<?php

namespace Vendorism\Sales\Providers;
use Illuminate\Support\ServiceProvider;


class SalesServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/views/sales', 'sales');
    }
}
