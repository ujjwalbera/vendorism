<?php
namespace Vendorism\Sales\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
use Mail;

use Vendorism\Sales\Models\Order;

class OrdersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @param  \Vendorism\Vendor\Repositories\RoleRepository  $role
     * @return void
     */
    public function __construct(/*Role $role*/)
    {
        Parent::__construct();
    }

    /**
     * @return void
     */
    public function admin_orders()
    {
        return view('sales::admin.orders');
    }



    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Vendor\Repositories\RoleRepository  $role
     * @return void
     */
    public function doLogin(Request $request)
    {
        //dd($request);
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $data = $request->validate($rules);

        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'username';

        $request->merge([
            $login_type => $request->input('username')
        ]);

        if (Auth::guard('vendor')->attempt($request->only($login_type, 'password'))) {
            //return redirect()->intended($this->redirectPath());
            $request->session()->flash('alert-success', 'Vendor was successful login!');
            return redirect()->route('vendor-dashboard');
        } else {
            $request->session()->flash('alert-warning', 'Invalid Credentials , Please try again.');
            return redirect()->route('vendor-login');
        }


    }



    /**
     * @return void
     */
    public function forgot()
    {

    }



    /**
     * @return void
     */
    public function registration()
    {
        if (Auth::guard('vendor')->check()) {
            return redirect()->route('vendor-dashboard');
        }
        return view('vendor::registration');
    }



    /**
     * @return void
     */
    public function doRegistration(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'email' => 'required|email|unique:' . $this->table,
            'username' => 'required|min:8|max:12|unique:' . $this->table,
            'password' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/',
            'password_confirmation' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/|same:password',
            'terms' => 'required'
        ];
        $data = $request->validate($rules);
        $vendor = new Vendor();
        $vendor->email = $request->input( 'email' );
        $vendor->username = $request->input( 'username' );
        $vendor->password = Hash::make( $request->input( 'password' ) );
        $vendor->role_id = 1;
        $vendor->remember_token = $request->input( '_token' );
        $vendor->save();
        $profile = new Profile();
        $profile->vendor_id = $vendor->id;
        $profile->name = $request->input( 'name' );
        $profile->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('alert-success', 'Vendor was successful registration!');
        return redirect()->back();
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function registration_confirmation(Request $request, $id)
    {
        $user = Admin::findOrFail($id);

        Mail::send('vendor::emails.confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from('ushadigitalsevakendra@gmail.com', 'Vendorism');

            $m->to($user->email, $user->name)->subject('Your registration confirmation!');
        });
    }

    /**
     * @return void
     */
    public function doLogout(Request $request)
    {
        Auth::guard('vendor')->logout();
        $request->session()->flash('alert-success', 'Vendor was successfully logged out!');
        return redirect()->route('vendor-login');
    }



    /**
     * @return void
     */
    public function dashboard()
    {
        //$id = Auth::user()->id;
        //DB::connection()->enableQueryLog();
        //$admin = User::find($id)->admin()->get();
        //$queries = \DB::getQueryLog();
        return view('vendor::dashboard');
    }





    /**
     * @return void
     */
    public function admin_carriers()
    {
        $carriers = Carrier::get();
        return view('carrier::admin.carriers', compact('carriers'));
    }

    /**
     * @return void
     */
    public function admin_vendor_add()
    {
        $vendors = Vendor::get();
        return view('vendor::admin.add_carrier', compact('vendors'));
    }

    /**
     * @return void
     */
    public function admin_vendor_store(Request $request)
    {
        $vendors = Vendor::get();
        return view('vendor::admin.vendors', compact('vendors'));
    }



    /**
     * @return void
     */
    public function admin_vendor_edit(Request $request, $vendor)
    {
        $vendors = Vendor::get();
        return view('vendor::admin.vendors', compact('vendors'));
    }




    /**
     * @return void
     */
    public function admin_vendor_update(Request $request, $vendor)
    {
        $vendors = Vendor::get();
        return view('vendor::admin.vendors', compact('vendors'));
    }


}
