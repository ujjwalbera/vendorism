@extends('layouts.admin.main')
@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Product Attribute Family Add</h3>
            <span class="float-right">
                <a href="<?php echo route('admin-attribute-families'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
        </div>
        <!-- /.card-header -->
        <form role="form" action="{{ route('admin-attribute-family-store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Family Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Attribute Family Name"  value="{{ old('name') }}">
                            @error('name') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Family Code</label>
                            <input type="text" name="code" class="form-control" placeholder="Attribute Family Code" value="{{ old('code') }}">
                            @error('code') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
                <button type="reset" class="btn btn-default float-right">Cancel</button>
            </div>
        </form>
    </div>
    <!-- /.card -->
@endsection
