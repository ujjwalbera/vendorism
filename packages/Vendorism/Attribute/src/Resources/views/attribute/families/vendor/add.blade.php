@extends('layouts.admin.main')
@section('content')
    <!-- Small boxes (Stat box) -->

    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Product Attribute Family Add</h3>
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>
                @if (Session::has('message'))
                    <div class="alert alert-warning">{{ Session::get('message') }}</div>
                @endif
                <form role="form" action="{{action('\Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_store')}}" method="post">
                    <!-- /.card-header -->
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Attribute Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Attribute name">
                                    @error('name') <div class="error"> {{ $message }} </div> @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control select2" name="status" data-placeholder="Select a Status" style="width: 100%;">
                                        <option value="1">Active</option>
                                        <option value="0">In-Active</option>
                                    </select>
                                    @error('status') <div class="error"> {{ $message }} </div> @enderror
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Attribute Code</label>
                                    <input type="text" name="code" class="form-control" placeholder="Attribute code">
                                    @error('code') <div class="error"> {{ $message }} </div> @enderror
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">

                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                    </div>
                </form>
                <!-- /.card-body -->
                <div class="card-footer">
                    <!--Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and
                    information about
                    the plugin.-->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
