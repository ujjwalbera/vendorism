@extends('layouts.admin.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Product Attribute listings</h3>
                <span class="float-right">
                    <a href="<?php echo route('admin-attribute-add'); ?>" class="btn btn-block btn-outline-primary btn-sm">Add New</a>
                </span>
            </div>
            <!-- /.card-header -->
            @php
                $i = 1
            @endphp
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#No</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Type</th>
                        <th>Validation</th>
                        <th>Visible</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($attributes as $attribute)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $attribute->admin_name }}</td>
                            <td>{{ $attribute->code }}</td>
                            <td>{{ $attribute->type }}</td>
                            <td>
                                @if($attribute->validation == true)
                                    <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Validation" title="Validation">
                                @else
                                    <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="Validation" title="Validation">
                                @endif
                            </td>
                            <td>
                                @if($attribute->is_visible_on_front == true)
                                    <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Visible" title="Visible">
                                @else
                                    <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="Visible" title="Visible">
                                @endif
                            </td>
                            <td>
                                @if($attribute->status == true)
                                    <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active" title="Active">
                                @else
                                    <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                         title="In-Active">
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin-attribute-edit', ['attribute' => $attribute->id]) }}">
                                    <i class="fas fa-edit"></i> Edit
                                </a>
                                <span class="button-divider">|</span>
                                <form action="{{ route('admin-attribute-delete', ['attribute' => $attribute->id]) }}" method="POST" class="delete-form">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="delete-form-button">
                                        <i class="fas fa-trash"></i> Delete
                                    </button >
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#No</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Validation</th>
                        <th>Visible</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row (main row) -->
@endsection
