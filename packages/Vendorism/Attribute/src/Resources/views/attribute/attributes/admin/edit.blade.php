@extends('layouts.admin.main')
@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Product Attribute Edit: : {{ $attribute->admin_name }}</h3>
            <span class="float-right">
                <a href="<?php echo route('admin-attributes'); ?>" class="btn btn-block
                btn-outline-primary
                btn-sm">Back</a>
            </span>
        </div>
        <!-- /.card-header -->
        <form role="form" action="{{ route('admin-attribute-update', ['attribute' => $attribute->id]) }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Admin Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Attribute Admin Name"
                                   value="{{ old('name', $attribute->admin_name) }}">
                            @error('name') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Type</label>
                            <select name="type" class="form-control select2" style="width: 100%;">
                                <option value="text" @if(old('type', $attribute->type) == 'text') selected @endif>Text</option>
                                <option value="select" @if(old('type', $attribute->type) == 'select') selected @endif>Select</option>
                                <option value="boolean" @if(old('type', $attribute->type) == 'boolean') selected @endif>Boolean</option>
                                <option value="textarea" @if(old('type', $attribute->type) == 'textarea') selected @endif>Textarea</option>
                                <option value="price" @if(old('type', $attribute->type) == 'price') selected @endif>Price</option>
                                <option value="date" @if(old('type', $attribute->type) == 'date') selected @endif>Date</option>
                                <option value="number" @if(old('type', $attribute->type) == 'number') selected @endif>Number</option>
                                <option value="email" @if(old('type', $attribute->type) == 'email') selected @endif>E-Mail</option>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group clearfix">
                            <label style="font-size: 14px; font-weight: 500; width: 100%;">Required</label>
                            <div class="icheck-success d-inline">
                                <input type="radio" name="is_required" value="1" id="required" @if(old('is_required', $attribute->is_required) == true) checked @endif>
                                <label for="required" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                            <div class="icheck-danger d-inline">
                                <input type="radio" name="is_required" value="0" id="not_required" @if(old('is_required', $attribute->is_required) == false) checked @endif>
                                <label for="not_required" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label style="font-size: 14px; font-weight: 500; width: 100%;">Visible in Frontend</label>
                            <div class="icheck-success d-inline">
                                <input type="radio" name="is_visible" value="1" id="visible" @if(old('is_visible', $attribute->is_visible_on_front) == true) checked @endif >
                                <label for="visible" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                            <div class="icheck-danger d-inline">
                                <input type="radio" name="is_visible" value="0" id="not_visible" @if(old('is_visible', $attribute->is_visible_on_front) == false) checked @endif>
                                <label for="not_visible" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Admin Code</label>
                            <input type="text" name="code" class="form-control" placeholder="Attribute Admin Code"
                                   value="{{ old('code', $attribute->code)}}">
                            @error('code') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Validation</label>
                            <select name="validation" class="form-control select2" style="width: 100%;">
                                <option value="accepted" @if(old('validation', $attribute->validation) == 'accepted') selected @endif>Accepted</option>
                                <option value="alpha" @if(old('validation', $attribute->validation) == 'alpha') selected @endif>Alphabetic</option>
                                <option value="alpha_dash" @if(old('validation', $attribute->validation) == 'alpha_dash') selected @endif>Alpha-Numeric</option>
                                <option value="alpha_num" @if(old('validation', $attribute->validation) == 'alpha_num') selected @endif>Must Alpha-Numeric</option>
                                <option value="boolean" @if(old('validation', $attribute->validation) == 'boolean') selected @endif>Boolean</option>
                                <option value="confirmed" @if(old('validation', $attribute->validation) == 'confirmed') selected @endif>Confirmed</option>
                                <option value="date" @if(old('validation', $attribute->validation) == 'date') selected @endif>Date</option>
                                <option value="email" @if(old('validation', $attribute->validation) == 'email') selected @endif>Email</option>
                                <option value="file" @if(old('validation', $attribute->validation) == 'file') selected @endif>File</option>
                                <option value="file" @if(old('validation', $attribute->validation) == 'files') selected @endif>Files</option>
                                <option value="image" @if(old('validation', $attribute->validation) == 'image') selected @endif>Image (jpeg, png, gif)</option>
                                <option value="image" @if(old('validation', $attribute->validation) == 'images') selected @endif>Images (jpeg, png, gif)</option>
                                <option value="integer" @if(old('validation', $attribute->validation) == 'integer') selected @endif>Integer</option>
                                <option value="ip" @if(old('validation', $attribute->validation) == 'ip') selected @endif>IP</option>
                                <option value="ipv4" @if(old('validation', $attribute->validation) == 'ipv4') selected @endif>ipv4</option>
                                <option value="ipv6" @if(old('validation', $attribute->validation) == 'ipv6') selected @endif>ipv6</option>
                                <option value="json" @if(old('validation', $attribute->validation) == 'json') selected @endif>Json</option>
                                <option value="nullable" @if(old('validation', $attribute->validation) == 'nullable') selected @endif>nullable</option>
                                <option value="numeric" @if(old('validation', $attribute->validation) == 'numeric') selected @endif>Numeric</option>
                                <option value="url" @if(old('validation', $attribute->validation) == 'url') selected @endif>URL</option>
                            </select>
                        </div>
                        <div class="form-group clearfix">
                            <label style="font-size: 14px; font-weight: 500; width: 100%;">Unique</label>
                            <div class="icheck-success d-inline">
                                <input type="radio" name="is_unique" value="1" id="unique" @if(old('is_unique', $attribute->is_unique) == true) checked @endif>
                                <label for="unique" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                            <div class="icheck-danger d-inline">
                                <input type="radio" name="is_unique" value="0" id="not_unique" @if(old('is_unique', $attribute->is_unique) == false) checked @endif>
                                <label for="not_unique" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label style="font-size: 14px; font-weight: 500; width: 100%;">Configurable</label>
                            <div class="icheck-success d-inline">
                                <input type="radio" name="is_configurable" value="1" id="configurable" @if(old('is_configurable', $attribute->is_configurable) == true) checked @endif >
                                <label for="configurable" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                            <div class="icheck-danger d-inline">
                                <input type="radio" name="is_configurable" value="0" id="not_configurable" @if(old('is_configurable', $attribute->is_configurable) ==  false) checked @endif >
                                <label for="not_configurable" style="font-size: 12px; font-weight: 500;">
                                </label>
                            </div>
                        </div>

                        <!-- /.form-group -->

                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
                <button type="reset" class="btn btn-default float-right">Cancel</button>
            </div>
        </form>
    </div>
    <!-- /.card -->
@endsection
