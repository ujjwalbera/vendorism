@extends('layouts.admin.main')
@section('content')
<!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
              <h3 class="card-title">Product Attribute Add</h3>
              <span class="float-right">
                <a href="<?php echo route('admin-attributes'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
          </div>
          <!-- /.card-header -->
          <form role="form" action="{{ route('admin-attribute-store') }}" method="post">
              @csrf
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label style="font-size: 14px; font-weight: 500;">Attribute Admin Name</label>
                      <input type="text" name="name" class="form-control" placeholder="Attribute Admin Name" value="{{ old('name') }}">
                      @error('name') <div class="input-error"> {{ $message }} </div> @enderror
                  </div>
                <div class="form-group">
                  <label style="font-size: 14px; font-weight: 500;">Attribute Type</label>
                  <select name="type" class="form-control select2" style="width: 100%;">
                      <option value="text" @if(old('type') == 'text') selected @endif>Text</option>
                      <option value="select" @if(old('type') == 'select') selected @endif>Select</option>
                      <option value="multi-select" @if(old('type') == 'multi-select') selected @endif>Multi-Select</option>
                      <option value="boolean" @if(old('type') == 'boolean') selected @endif>Boolean</option>
                      <option value="radio" @if(old('type') == 'radio') selected @endif>Radio Buttons</option>
                      <option value="checkbox" @if(old('type') == 'checkbox') selected @endif>Checkbox</option>
                      <option value="textarea" @if(old('type') == 'textarea') selected @endif>Textarea</option>
                      <option value="price" @if(old('type') == 'price') selected @endif>Price</option>
                      <option value="date" @if(old('type') == 'date') selected @endif>Date</option>
                      <option value="number" @if(old('type') == 'number') selected @endif>Number</option>
                      <option value="email" @if(old('type') == 'email') selected @endif>E-Mail</option>
                      <option value="file" @if(old('type') == 'file') selected @endif>File</option>
                      <option value="files" @if(old('type') == 'files') selected @endif>Files</option>
                      <option value="image" @if(old('type') == 'image') selected @endif>Image</option>
                      <option value="images" @if(old('type') == 'images') selected @endif>Images</option>
                  </select>
                </div>
                <!-- /.form-group -->
                  <div class="form-group clearfix">
                      <label style="font-size: 14px; font-weight: 500; width: 100%;">Required</label>
                      <div class="icheck-success d-inline">
                          <input type="radio" name="is_required" id="required" value="1" @if(old('is_required') == 1) checked @endif>
                          <label for="required" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                      <div class="icheck-danger d-inline">
                          <input type="radio" name="is_required" id="not_required" value="0" @if(old('is_required') == 0) checked @endif>
                          <label for="not_required" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                  </div>
                  <div class="form-group clearfix">
                      <label style="font-size: 14px; font-weight: 500; width: 100%;">Visible in Frontend</label>
                      <div class="icheck-success d-inline">
                          <input type="radio" name="is_visible" id="visible" value="1" @if(old('is_visible') == 1) checked @endif>
                          <label for="visible" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                      <div class="icheck-danger d-inline">
                          <input type="radio" name="is_visible" id="not_visible" value="0" @if(old('is_visible') == 0) checked @endif>
                          <label for="not_visible" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                  </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                  <div class="form-group">
                      <label style="font-size: 14px; font-weight: 500;">Attribute Admin Code</label>
                      <input type="text" name="code" class="form-control" placeholder="Attribute Admin Code" value="{{ old('code') }}">
                      @error('code') <div class="input-error"> {{ $message }} </div> @enderror
                  </div>
                  <div class="form-group">
                      <label style="font-size: 14px; font-weight: 500;">Validation</label>
                      <select name="validation" class="form-control select2" style="width: 100%;">
                          <option value="accepted" @if(old('validation') == 'accepted') selected @endif>Accepted</option>
                          <option value="alpha" @if(old('validation') == 'alpha') selected @endif>Alphabetic</option>
                          <option value="alpha_dash" @if(old('validation') == 'alpha_dash') selected @endif>Alpha-Numeric</option>
                          <option value="alpha_num" @if(old('validation') == 'alpha_num') selected @endif>Must Alpha-Numeric</option>
                          <option value="boolean" @if(old('validation') == 'boolean') selected @endif>Boolean</option>
                          <option value="confirmed" @if(old('validation') == 'confirmed') selected @endif>Confirmed</option>
                          <option value="date" @if(old('validation') == 'date') selected @endif>Date</option>
                          <option value="email" @if(old('validation') == 'email') selected @endif>Email</option>
                          <option value="file" @if(old('validation') == 'file') selected @endif>File</option>
                          <option value="files" @if(old('validation') == 'file') selected @endif>Files</option>
                          <option value="image" @if(old('validation') == 'image') selected @endif>Image (jpeg, png, gif)</option>
                          <option value="images" @if(old('validation') == 'image') selected @endif>Images (jpeg, png, gif)</option>
                          <option value="integer" @if(old('validation') == 'integer') selected @endif>Integer</option>
                          <option value="ip" @if(old('validation') == 'ip') selected @endif>IP</option>
                          <option value="ipv4" @if(old('validation') == 'ipv4') selected @endif>ipv4</option>
                          <option value="ipv6" @if(old('validation') == 'ipv6') selected @endif>ipv6</option>
                          <option value="json" @if(old('validation') == 'json') selected @endif>Json</option>
                          <option value="nullable" @if(old('validation') == 'nullable') selected @endif>nullable</option>
                          <option value="numeric" @if(old('validation') == 'numeric') selected @endif>Numeric</option>
                          <option value="url" @if(old('validation') == 'url') selected @endif>URL</option>
                      </select>
                  </div>
                  <div class="form-group clearfix">
                      <label style="font-size: 14px; font-weight: 500; width: 100%;">Unique</label>
                      <div class="icheck-success d-inline">
                          <input type="radio" name="is_unique" id="unique" value="1" @if(old('is_unique') == 1) checked @endif>
                          <label for="unique" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                      <div class="icheck-danger d-inline">
                          <input type="radio" name="is_unique" id="not_unique" value="0" @if(old('is_unique') == 0) checked @endif>
                          <label for="not_unique" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                  </div>
                  <div class="form-group clearfix">
                      <label style="font-size: 14px; font-weight: 500; width: 100%;">Configurable</label>
                      <div class="icheck-success d-inline">
                          <input type="radio" name="is_configurable" id="configurable" value="1" @if(old('not_configurable') == 1) checked @endif>
                          <label for="configurable" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                      <div class="icheck-danger d-inline">
                          <input type="radio" name="is_configurable" id="not_configurable" value="0" @if(old('not_configurable') == 0) checked @endif>
                          <label for="not_configurable" style="font-size: 12px; font-weight: 500;">
                          </label>
                      </div>
                  </div>

                <!-- /.form-group -->

                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
              <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
              <button type="reset" class="btn btn-default float-right">Cancel</button>
          </div>
            </form>
        </div>
        <!-- /.card -->
@endsection
