@extends('layouts.vendor.main')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Product Attribute listings</h5>
                    <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#Position</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Type</th>
                                    <th>Validation</th>
                                    <th>Visible</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($attributes as $attribute)
                                    <tr>
                                        <td>{{ $attribute->position }}</td>
                                        <td>{{ $attribute->admin_name }}</td>
                                        <td>{{ $attribute->code }}</td>
                                        <td>{{ $attribute->type }}</td>
                                        <td>
                                            @if($attribute->validation == true)
                                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Validation" title="Validation">
                                            @else
                                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="Validation" title="Validation">
                                            @endif
                                        </td>
                                        <td>
                                            @if($attribute->is_visible_on_front == true)
                                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Visible" title="Visible">
                                            @else
                                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="Visible" title="Visible">
                                            @endif
                                        </td>
                                        <td>
                                            @if($attribute->status == true)
                                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active" title="Active">
                                            @else
                                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                                     title="In-Active">
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#">
                                                <i class="fas fa-edit"></i> View
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#Position</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Validation</th>
                                    <th>Visible</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
<!-- Small boxes (Stat box) -->

@endsection
