@extends('layouts.admin.main')
@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Product Attribute Option Edit: {{ $attributeOption->name }}</h3>
            <span class="float-right">
                <a href="<?php echo route('admin-attribute-options'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
        </div>
        <!-- /.card-header -->
        <form role="form" action="{{ route('admin-attribute-option-update', ['option' => $attributeOption->id]) }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Type</label>
                            <select name="attribute" class="form-control select2" style="width: 100%;">
                                @foreach($attributes as $attribute)
                                    <option value="{{ $attribute->id }}" @if($attributeOption->attribute_id == $attribute->id) selected @endif>{{ $attribute->admin_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Option Code</label>
                            <input type="text" name="code" class="form-control" placeholder="Attribute Option Code"
                                   value="{{ $attributeOption->code }}">
                            @error('code') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Option Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Attribute Option Name" value="{{ $attributeOption->frontend_name }}">
                            @error('name') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>

                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
                <button type="reset" class="btn btn-default float-right">Cancel</button>
            </div>
        </form>
    </div>
    <!-- /.card -->
@endsection
