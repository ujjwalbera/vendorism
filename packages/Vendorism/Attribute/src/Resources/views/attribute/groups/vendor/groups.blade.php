@extends('layouts.vendor.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Product Attribute Group listings</h3>
                <span class="float-right">
                    <a href="<?php echo route('admin-attribute-family-add'); ?>" class="btn btn-block
                    btn-outline-primary btn-sm">Add New</a>
                </span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Position</th>
                        <th>Name</th>
                        <th>Family</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($attributeGruops as $attributeGruop)
                    <tr>
                        <td>{{ $attributeGruop->id }}</td>
                        <td>{{ $attributeGruop->position }}</td>
                        <td>{{ $attributeGruop->name }}</td>
                        <td>{{ $attributeGruop->attribute_family->name }}</td>
                        <td>
                            @if($attributeGruop->status == true)
                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active" title="Active">
                            @else
                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                     title="In-Active">
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin-attribute-family-edit') . '/' . $attributeGruop->id }}">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                             /
                            <a href="{{ route('admin-attribute-family-delete') . '/' . $attributeGruop->id }}" style="color:#ff0000;">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Position</th>
                        <th>Name</th>
                        <th>Family</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row (main row) -->
@endsection
