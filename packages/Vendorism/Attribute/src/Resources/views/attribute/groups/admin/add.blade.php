@extends('layouts.admin.main')
@section('content')
    <!-- SELECT2 EXAMPLE -->
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Product Attribute Gruop Add</h3>
            <span class="float-right">
                <a href="<?php echo route('admin-attribute-groups'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
        </div>
        <!-- /.card-header -->
        <form role="form" action="{{ route('admin-attribute-group-store') }}" method="post">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Gruop Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Attribute Gruop Name" value="{{ old('name') }}" >
                            @error('name') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Family</label>
                            <select name="family" class="form-control select2" style="width: 100%;">
                                @foreach($attributeFamilies as $attributeFamily)
                                <option value="{{ $attributeFamily->id }}" @if(old('family') == $attributeFamily->id) selected @endif>{{ $attributeFamily->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 14px; font-weight: 500;">Attribute Gruop Code</label>
                            <input type="text" name="code" class="form-control" placeholder="Attribute Gruop Code" value="{{ old('code') }}" >
                            @error('code') <div class="input-error"> {{ $message }} </div> @enderror
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-success">Save <i class="fas
                    fa-arrow-circle-right"></i></button>
                <button type="reset" class="btn btn-default float-right">Cancel</button>
            </div>
        </form>
    </div>
    <!-- /.card -->
@endsection
