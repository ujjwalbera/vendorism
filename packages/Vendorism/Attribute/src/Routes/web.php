<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('admin/attributes', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_list')->middleware
    ('admin_loggedin')->name('admin-attributes');
    Route::get('admin/attribute/add', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_create')->middleware('admin_loggedin')->name('admin-attribute-add');
    Route::post('admin/attribute/store', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_store')->middleware('admin_loggedin')->name('admin-attribute-store');
    Route::get('admin/attribute/edit/{attribute}', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_edit')->middleware('admin_loggedin')->name('admin-attribute-edit');
    Route::post('admin/attribute/update/{attribute}', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_update')->middleware('admin_loggedin')->name('admin-attribute-update');
    Route::delete('admin/attribute/delete/{attribute}', 'Vendorism\Attribute\Http\Controllers\AttributeController@admin_destroy')->middleware('admin_loggedin')->name('admin-attribute-delete');

    Route::get('admin/attribute-families', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_list')->middleware('admin_loggedin')->name('admin-attribute-families');
    Route::get('admin/attribute-family/add', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_create')->middleware('admin_loggedin')->name('admin-attribute-family-add');
    Route::post('admin/attribute-family/store', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_store')->middleware('admin_loggedin')->name('admin-attribute-family-store');
    Route::get('admin/attribute-family/edit/{family}', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_edit')->middleware('admin_loggedin')->name('admin-attribute-family-edit');
    Route::post('admin/attribute-family/update/{family}', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_update')->middleware('admin_loggedin')->name('admin-attribute-family-update');
    Route::delete('admin/attribute-family/delete/{family}', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@admin_destroy')->middleware('admin_loggedin')->name('admin-attribute-family-delete');

    Route::get('admin/attribute-groups', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_list')->middleware('admin_loggedin')->name('admin-attribute-groups');
    Route::get('admin/attribute-group/add', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_create')->middleware('admin_loggedin')->name('admin-attribute-group-add');
    Route::post('admin/attribute-group/store', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_store')->middleware('admin_loggedin')->name('admin-attribute-group-store');
    Route::get('admin/attribute-group/edit/{group}', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_edit')->middleware('admin_loggedin')->name('admin-attribute-group-edit');
    Route::post('admin/attribute-group/update/{group}', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_update')->middleware('admin_loggedin')->name('admin-attribute-group-update');
    Route::delete('admin/attribute-group/delete/{group}', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@admin_destroy')->middleware('admin_loggedin')->name('admin-attribute-group-delete');

    Route::get('admin/attribute-options', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_list')->middleware('admin_loggedin')->name('admin-attribute-options');
    Route::get('admin/attribute-option/add', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_create')->middleware('admin_loggedin')->name('admin-attribute-option-add');
    Route::post('admin/attribute-option/store', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_store')->middleware('admin_loggedin')->name('admin-attribute-option-store');
    Route::get('admin/attribute-option/edit/{option}', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_edit')->middleware('admin_loggedin')->name('admin-attribute-option-edit');
    Route::post('admin/attribute-option/update/{option}', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_update')->middleware('admin_loggedin')->name('admin-attribute-option-update');
    Route::delete('admin/attribute-option/delete/{option}', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@admin_destroy')->middleware('admin_loggedin')->name('admin-attribute-option-delete');

    Route::get('vendor/attributes', 'Vendorism\Attribute\Http\Controllers\AttributeController@vendor_list')->middleware('vendor_loggedin')->name('vendor-attributes');
    Route::get('vendor/attribute-families', 'Vendorism\Attribute\Http\Controllers\AttributeFamilyController@vendor_list')->middleware('vendor_loggedin')->name('vendor-attribute-families');
    Route::get('vendor/attribute-groups', 'Vendorism\Attribute\Http\Controllers\AttributeGroupController@vendor_list')->middleware('vendor_loggedin')->name('vendor-attribute-groups');
    Route::get('vendor/attribute-options', 'Vendorism\Attribute\Http\Controllers\AttributeOptionController@vendor_list')->middleware('vendor_loggedin')->name('vendor-attribute-options');

});

