<?php

namespace Vendorism\Attribute\Providers;

use Illuminate\Support\ServiceProvider;

class AttributeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/views/attribute', 'attribute');
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/attribute'),
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/role'),
        ]);*/
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
