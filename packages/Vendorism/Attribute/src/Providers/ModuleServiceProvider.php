<?php

namespace Vendorism\Attribute\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Vendorism\Attribute\Models\Attribute::class,
        \Vendorism\Attribute\Models\AttributeFamily::class,
        \Vendorism\Attribute\Models\AttributeGroup::class,
        \Vendorism\Attribute\Models\AttributeOption::class,
        \Vendorism\Attribute\Models\AttributeOptionTranslation::class,
        \Vendorism\Attribute\Models\AttributeTranslation::class,
    ];
}
