<?php

namespace Vendorism\Attribute\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Vendorism\Attribute\Models\Attribute;
use Illuminate\Validation\Rule;
use Event;

/**
 * Catalog attribute controller
 *
 * @author    Ujjwal Bera <ushaservices@webkul.com>
 * @copyright 2019 Usha Digital Services (http://www.ushadigitalservices.com)
 */
class AttributeController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * AttributeRepository object
     *
     * @var array
     */
    protected $attribute;

    /**
     * Create a new controller instance.
     *
     * @param  \Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('HERE');
        return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        $attributes = Attribute::all();
        return view('attribute::attributes.admin.attributes', compact('attributes'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        $attributes = Attribute::all();
        //dd(Auth::guard('vendor')->user()->id);
        return view('attribute::attributes.vendor.attributes', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_create()
    {
        return view('attribute::attributes.admin.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function vendor_create()
//    {
//        return view('attribute::attributes.vendor.add');
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_store(Request $request)
    {
        $customMessages = [
            'code.required' => 'Attribute Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Code minimum length should be 3.',
            'code.max' => 'Attribute Code maximum length should be 30.',
            'code.unique' => 'Attribute Code should be unique.',
            'name.required' => 'Attribute Name is required.',
            'name.regex' => 'Invalid format in Attribute Name. Only letters, numbers and space are allowed.',
            'type.required' => 'Attribute Type is required.',
            'validation.required' => 'Attribute Validation Type is required.',
        ];

        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:attributes',
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/',
            'type' => 'required',
            'validation' => 'required'
        ], $customMessages);
        $attribute = new Attribute();
        $attribute->code = $request->input( 'code' );
        $attribute->admin_name = $request->input( 'name' );
        $attribute->type = $request->input( 'type' );
        $attribute->validation = $request->input( 'validation' );
        $attribute->status = true;
        $attribute->is_required = $request->input( 'is_required' );
        $attribute->is_unique = $request->input( 'is_unique' );
        $attribute->is_configurable = $request->input( 'is_configurable' );
        $attribute->is_visible_on_front = $request->input( 'is_visible' );
        $attribute->save();
        $request->session()->flash('alert-success', 'Attribute was successful added!');
        return redirect()->route('admin-attributes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = $this->attribute->findOrFail($id);

        return view($this->_config['view'], compact('attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_edit(Request $request, $id)
    {
        $attribute = Attribute::where('id', $id)->first();
        //dd($attribute->toArray());
        return view('attribute::attributes.admin.edit', compact('attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function vendor_edit($id)
//    {
//        $attribute = Attribute::where('id', $id)->first();
//        return view('attribute::attributes.vendor.edit', compact('attribute'));
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_update(Request $request, $id)
    {
        $attribute = Attribute::where('id', $id)->first();
        $customMessages = [
            'code.required' => 'Attribute Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Code minimum length should be 3.',
            'code.max' => 'Attribute Code maximum length should be 30.',
            'code.unique' => 'Attribute Code should be unique.',
            'name.required' => 'Attribute Name is required.',
            'name.regex' => 'Invalid format in Attribute Name. Only letters and numbers are allowed.',
            'type.required' => 'Attribute Type is required.',
            'validation.required' => 'Attribute Validation Type is required.',
        ];
        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:attributes,code,'.$attribute->id.',id',
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/',
            'type' => 'required',
            'validation' => 'required'
        ]);

        $attribute->code = $request->input( 'code' );
        $attribute->admin_name = $request->input( 'name' );
        $attribute->type = $request->input( 'type' );
        $attribute->validation = $request->input( 'validation' );
        $attribute->status = true;
        $attribute->is_user_defined = true;
        $attribute->is_required = $request->input( 'is_required' );
        $attribute->is_unique = $request->input( 'is_unique' );
        $attribute->is_configurable = $request->input( 'is_configurable' );
        $attribute->is_visible_on_front = $request->input('is_visible') == '1' ? true : false;
        $attribute->save();
        $request->session()->flash('alert-success', 'Attribute was successful updated!');
        return redirect()->route('admin-attributes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_destroy(Request $request, $id)
    {
        $attribute = Attribute::findOrFail($id);

        if (! $attribute->is_user_defined) {
            $request->session()->flash('alert-info', 'You can\'t delete core system attribute.');
        } elseif (count($attribute->options)){
            $request->session()->flash('alert-info', 'Attribute have option values. Please delete options before delete the attribute.');
        } else {
            try {
                $attribute->delete();
                if ($attribute->trashed()) {
                    //some logic
                }
                $request->session()->flash('alert-success', 'Attribute was successful deleted!');
            } catch(\Exception $e) {
                $request->session()->flash('alert-error', 'Unable to delete the attribute. Please try again!');
            }
        }
        return redirect()->route('admin-attributes');
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy()
    {
        $suppressFlash = false;

        if (request()->isMethod('post')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                $attribute = $this->attribute->find($value);

                try {
                    if (! $attribute->is_user_defined) {
                        continue;
                    } else {
                        $this->attribute->delete($value);
                    }
                } catch (\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success', ['resource' => 'attributes']));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'attributes']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }


    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $code = Str::slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($code, $id);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('code', $code)){
            return $code;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $code.'-'.$i;
            if (! $allSlugs->contains('code', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique code');
    }


    protected function getRelatedSlugs($code, $id = 0)
    {
        return Attribute::select('code')->where('code', 'like', $code.'%')->where('id', '<>', $id)->get();
    }
}
