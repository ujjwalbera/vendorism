<?php

namespace Vendorism\Attribute\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

use Vendorism\Attribute\Models\AttributeFamily;


/**
 * Catalog family controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AttributeFamilyController extends Controller
{

    protected $table = 'attribute_families';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        $attributeFamilies = AttributeFamily::all();
        return view('attribute::families.admin.families', compact('attributeFamilies'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        $attributeFamilies = AttributeFamily::all();
        return view('attribute::families.vendor.families', compact('attributeFamilies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_create(Request $request)
    {
        return view('attribute::families.admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_store(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|unique:' . $this->table,
            'name' => 'required|regex:/^[A-Za-z ]+$/',
        ]);
        $attributeFamily = new AttributeFamily();
        $attributeFamily->code = $this->createSlug($request->input( 'code' ));
        $attributeFamily->name = $request->input( 'name' );
        $attributeFamily->status = 1;
        $attributeFamily->is_user_defined = 1;
        $attributeFamily->save();
        $request->session()->flash('alert-success', 'Attribute Family was successful added!');
        return redirect()->route('admin-attribute-families');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_edit(Request $request, $id)
    {
        $attributeFamily = AttributeFamily::where('id', $id)->first();
        //dd($attributeFamily->toArray());
        return view('attribute::families.admin.edit', compact('attributeFamily'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_update(Request $request, $id)
    {
        $attributeFamily = AttributeFamily::where('id', $id)->first();
        $request->validate([
            'code' => 'required|min:3|max:20|unique:'. $this->table.',code,'.$attributeFamily->id.',id',
            'name' => 'required'
        ]);

        $attributeFamily->code = $request->input( 'code' );
        $attributeFamily->name = $request->input( 'name' );
        $attributeFamily->save();
        $request->session()->flash('alert-success', 'Attribute Family was successful updated!');
        return redirect()->route('admin-attribute-families');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_destroy(Request $request, $id)
    {
        $attributeFamily = AttributeFamily::findOrFail($id);

        if ($attributeFamily->count() == 1) {
            $request->session()->flash('alert-info', 'You can\'t delete core system Attribute Family.');
        } else if ($attributeFamily->attribute_groups()->count()) {
            $request->session()->flash('alert-info', 'Attribute Family have groups. Please delete groups before delete the attribute family.');
        } else if ($attributeFamily->products()->count()) {
            $request->session()->flash('alert-info', 'Attribute Family have products. Please delete products before delete the attribute family.');
        } else {
            try {
                $attributeFamily->delete($id);
                $request->session()->flash('alert-success', 'Attribute Family was successful deleted!');
            } catch (\Exception $e) {
                $request->session()->flash('alert-error', 'Unable to delete the attribute family. Please try again!');
            }
        }

        return redirect()->route('admin-attribute-families');
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('delete')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    $this->attributeFamily->delete($value);
                } catch (\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', ('admin::app.datagrid.mass-ops.delete-success'));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Attribute Family']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }


    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public function createSlug(string $title = null, int $id = 0)
    {
        // Normalize the title
        $code = Str::slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($code, $id);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('code', $code)){
            return $code;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $code.'-'.$i;
            if (! $allSlugs->contains('code', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique code');
    }


    protected function getRelatedSlugs(string $code = null, int $id = 0)
    {
        return AttributeFamily::select('code')->where('code', 'like', "{$code}%")->where('id', '<>', $id)->get();
    }
}
