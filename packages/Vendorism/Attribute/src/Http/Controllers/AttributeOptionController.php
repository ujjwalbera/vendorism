<?php

namespace Vendorism\Attribute\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

use Vendorism\Attribute\Models\Attribute;
use Vendorism\Attribute\Models\AttributeOption;


/**
 * Catalog family controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AttributeOptionController extends Controller
{

    protected $table = 'attribute_options';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        $attributeOptions = AttributeOption::with('attribute')->get();
        //dd($attributeOptions->toArray());
        //dd($attributeOptions);
        return view('attribute::options.admin.options', compact('attributeOptions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        $attributeOptions = AttributeOption::with('attribute')->get();
        //dd($attributeOptions->toArray());
        //dd($attributeOptions);
        return view('attribute::options.vendor.options', compact('attributeOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @return \Illuminate\Http\Response
     */
    public function admin_create()
    {
        $attributes = Attribute::whereIn('type', ['select', 'multi-select', 'boolean', 'radio', 'checkbox'])->get();
        return view('attribute::options.admin.add', compact('attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_store(Request $request)
    {
        $customMessages = [
            'code.required' => 'Attribute Group Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Group Code minimum length should be 3.',
            'code.max' => 'Attribute Group Code maximum length should be 30.',
            'code.unique' => 'Attribute Group Code should be unique.',
            'name.required' => 'Attribute Group Name is required.',
            'name.regex' => 'Invalid format in Attribute Group Name. Only letters, numbers and space are allowed.',
        ];

        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:'. $this->table,
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/'
        ], $customMessages);


        $SortOrderObj = AttributeOption::where('attribute_id', $request->input( 'attribute' ))->orderBy('sort_order', 'desc')->first();
        $sortOrderPoint = $SortOrderObj->sort_order + 1;
        $attributeOption = new AttributeOption();
        $attributeOption->code = $request->input( 'code' );
        $attributeOption->frontend_name = $request->input( 'name' );
        $attributeOption->attribute_id = $request->input( 'attribute' );
        $attributeOption->sort_order = $sortOrderPoint;
        $attributeOption->save();
        $request->session()->flash('alert-success', 'Attribute Option was successful added!');
        return redirect()->route('admin-attribute-options');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_edit(Request $request, $id)
    {
        $attributeOption = AttributeOption::where('id', $id)->first();
        $attributes = Attribute::whereIn('type', ['select', 'multi-select', 'boolean', 'radio', 'checkbox'])->get();

        return view('attribute::options.admin.edit', compact('attributeOption', 'attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_update(Request $request, $id)
    {
        $attributeOption = AttributeOption::findOrFail($id);
        $customMessages = [
            'code.required' => 'Attribute Group Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Group Code minimum length should be 3.',
            'code.max' => 'Attribute Group Code maximum length should be 30.',
            'code.unique' => 'Attribute Group Code should be unique.',
            'name.required' => 'Attribute Group Name is required.',
            'name.regex' => 'Invalid format in Attribute Group Name. Only letters, numbers and space are allowed.',
        ];

        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:'. $this->table.',code,'.$attributeOption->id.',id',
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/'
        ], $customMessages);

        $attribute_id = $request->input( 'attribute' );

        if( $attributeOption->attribute_id != $attribute_id ) {
            $SortOrderObj = AttributeOption::where('attribute_id', $attribute_id)->orderBy('sort_order', 'desc')->first();
            if($SortOrderObj) {
                $sortOrderPoint = $SortOrderObj->sort_order + 1;
            } else {
                $sortOrderPoint = 1;
            }

            $attributeOption->sort_order = $sortOrderPoint;
        }

        $attributeOption->code = $request->input( 'code' );
        $attributeOption->frontend_name = $request->input( 'name' );
        $attributeOption->attribute_id = $request->input( 'attribute' );
        $attributeOption->save();
        $request->session()->flash('alert-success', 'Attribute Option was successful updated!');
        return redirect()->route('admin-attribute-options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_destroy(Request $request, $id)
    {
        $attributeOption = AttributeOption::findOrFail($id);

        if ($attributeOption->count() == 1) {
            $request->session()->flash('alert-info', 'You can\'t delete core system Attribute Option.');
        //} else if ($attributeOption->attribute()->count()) {
            //$request->session()->flash('alert-info', 'Attribute Option have attributes. Please delete attributes before delete the attribute group.');
            //} else if ($attributeGroup->products()->count()) {
            //$request->session()->flash('alert-info', 'Attribute Group have products. Please delete products before delete the attribute group.');
        } else {
            try {
                $attributeOption->delete($id);
                $request->session()->flash('alert-success', "Attribute Option '".$attributeOption->frontend_name."' was successful deleted!");
            } catch (\Exception $e) {
                $request->session()->flash('alert-error', "Unable to delete the attribute option '".$attributeOption->frontend_name."'. Please try again!");
            }
        }
        return redirect()->route('admin-attribute-options');
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('delete')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    $this->attributeFamily->delete($value);
                } catch (\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', ('admin::app.datagrid.mass-ops.delete-success'));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Attribute Family']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}
