<?php

namespace Vendorism\Attribute\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

use Vendorism\Attribute\Models\AttributeFamily;
use Vendorism\Attribute\Models\AttributeGroup;


/**
 * Catalog family controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class AttributeGroupController extends Controller
{

    protected $table = 'attribute_groups';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        $attributeGruops = AttributeGroup::with('attribute_family')->orderBy('attribute_family_id', 'ASC')->orderBy('position', 'ASC')->get();
        //dd($attributeGruops->toArray());
        return view('attribute::groups.admin.groups', compact('attributeGruops'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        $attributeGruops = AttributeGroup::with('attribute_family')->get();
        //dd($attributeGruops->toArray());
        return view('attribute::groups.vendor.groups', compact('attributeGruops'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @return \Illuminate\Http\Response
     */
    public function admin_create()
    {
        $attributeFamilies = AttributeFamily::where('status', true)->get();
        //dd($attributeFamilies->toArray());
        return view('attribute::groups.admin.add', compact('attributeFamilies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_store(Request $request)
    {
        $customMessages = [
            'code.required' => 'Attribute Group Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Group Code minimum length should be 3.',
            'code.max' => 'Attribute Group Code maximum length should be 30.',
            'code.unique' => 'Attribute Group Code should be unique.',
            'name.required' => 'Attribute Group Name is required.',
            'name.regex' => 'Invalid format in Attribute Group Name. Only letters, numbers and space are allowed.',
        ];

        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:'. $this->table,
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/'
        ], $customMessages);

        $attributeGroup = new AttributeGroup();
        $attributeGroup->code = $request->input( 'code' );
        $attributeGroup->name = $request->input( 'name' );
        $attributeGroup->attribute_family_id = $request->input( 'family' );
        $attributeGroup->status = 1;
        $attributeGroup->is_user_defined = 1;
        $attributeGroup->save();
        $request->session()->flash('alert-success', 'Attribute Group was successful added!');
        return redirect()->route('admin-attribute-groups');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vendorism\Attribute\Repositories\AttributeRepository  $attribute
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_edit(Request $request, $id)
    {
        $attributeGroup = AttributeGroup::where('id', $id)->first();
        $attributeFamilies = AttributeFamily::all(['id', 'name']);

        return view('attribute::groups.admin.edit', compact('attributeFamilies', 'attributeGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_update(Request $request, $id)
    {
        $attributeGroup = AttributeGroup::findOrFail($id);
        //print_r($attributeGroup->toArray()); die;
        $customMessages = [
            'code.required' => 'Attribute Group Code is required.',
            'code.regex' => 'Invalid format in Attribute Code. Only lowercase letters, numbers and _ are allowed.',
            'code.min' => 'Attribute Group Code minimum length should be 3.',
            'code.max' => 'Attribute Group Code maximum length should be 30.',
            'code.unique' => 'Attribute Group Code should be unique.',
            'name.required' => 'Attribute Group Name is required.',
            'name.regex' => 'Invalid format in Attribute Group Name. Only letters, numbers and space are allowed.',
        ];

        $request->validate([
            'code' => 'required|min:3|max:30|regex:/^[a-z0-9_]*$/|unique:'. $this->table.',code,'.$attributeGroup->id.',id',
            'name' => 'required|regex:/^[a-zA-Z0-9 ]*$/'
        ], $customMessages);

        $attributeGroup->code = $request->input( 'code' );
        $attributeGroup->name = $request->input( 'name' );
        $attributeGroup->attribute_family_id = $request->input( 'family' );
        $attributeGroup->status = 1;
        $attributeGroup->is_user_defined = 1;
        $attributeGroup->save();
        $request->session()->flash('alert-success', 'Attribute Group was successful updated!');
        return redirect()->route('admin-attribute-groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function admin_destroy(Request $request, $id)
    {
        $attributeGroup = AttributeGroup::findOrFail($id);

        if ($attributeGroup->count() == 1) {
            $request->session()->flash('alert-info', 'You can\'t delete core system Attribute Group.');
        } else if ($attributeGroup->group_attributes()->count()) {
            $request->session()->flash('alert-info', 'Attribute Group have attributes. Please delete attributes before delete the attribute group.');
        //} else if ($attributeGroup->products()->count()) {
            //$request->session()->flash('alert-info', 'Attribute Group have products. Please delete products before delete the attribute group.');
        } else {
            try {
                $attributeGroup->delete($id);
                $request->session()->flash('alert-success', 'Attribute Group was successful deleted!');
            } catch (\Exception $e) {
                $request->session()->flash('alert-error', 'Unable to delete the attribute group. Please try again!');
            }
        }
        return redirect()->route('admin-attribute-groups');
    }

    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('delete')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    $this->attributeFamily->delete($value);
                } catch (\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', ('admin::app.datagrid.mass-ops.delete-success'));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Attribute Family']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}
