<?php

namespace Vendorism\Attribute\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Vendorism\Product\Models\Product;
use Vendorism\Attribute\Models\Attribute;


class AttributeFamily extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    protected $table = 'attribute_families';

    protected $fillable = ['id', 'name'];

    /**
     * Get all of the attributes for the attribute groups.
     */
    public function group_attributes()
    {
        return (Attribute::modelClass())::join('attribute_group_mappings', 'attributes.id', '=', 'attribute_group_mappings.attribute_id')
            ->join('attribute_groups', 'attribute_group_mappings.attribute_group_id', '=', 'attribute_groups.id')
            ->join('attribute_families', 'attribute_groups.attribute_family_id', '=', 'attribute_families.id')
            ->where('attribute_families.id', $this->id)
            ->select('attributes.*');
    }

    /**
     * Get all of the attributes for the attribute groups.
     */
    public function getCustomAttributesAttribute()
    {
        return $this->group_attributes()->get();
    }

    /**
     * Get all of the attribute groups.
     */
    public function attribute_groups()
    {
        return $this->hasMany('Vendorism\Attribute\Models\AttributeGroup')->orderBy('position');
    }

    /**
     * Get all of the attributes for the attribute groups.
     */
    public function getConfigurableAttributesAttribute()
    {
        return $this->group_attributes()->where('attributes.is_configurable', 1)->where('attributes.type', 'select')->get();
    }

    /**
     * Get all of the products.
     */
    public function products()
    {
        return $this->hasMany('Vendorism\Product\Models\Product');
    }
}
