<?php

namespace Vendorism\Attribute\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use SoftDeletes;
    
    public $translatedAttributes = ['name'];

    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    protected $fillable = ['code', 'admin_name', 'type', 'position', 'is_required', 'is_unique', 'validation', 'value_per_locale', 'value_per_channel', 'is_filterable', 'is_configurable', 'is_visible_on_front', 'is_user_defined', 'swatch_type', 'use_in_flat'];

    // protected $with = ['options'];

    protected $casts = [
        'status' => 'boolean',
        'is_required' => 'boolean',
        'is_unique' => 'boolean',
        'is_configurable' => 'boolean',
        'is_visible_on_front' => 'boolean',
    ];

    /**
     * Get the options.
     */
    public function options()
    {
        return $this->hasMany('Vendorism\Attribute\Models\AttributeOption');
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterableAttributes($query)
    {
        return $query->where('is_filterable', 1)->where('swatch_type', '<>', 'image')->orderBy('position');
    }
}
