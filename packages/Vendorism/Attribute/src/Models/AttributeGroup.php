<?php

namespace Vendorism\Attribute\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeGroup extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    protected $fillable = ['name', 'position', 'is_user_defined'];

    /**
     * Get the attributes that owns the attribute group.
     */
    public function group_attributes()
    {
        return $this->belongsToMany('Vendorism\Attribute\Models\Attribute', 'attribute_group_mappings')
            ->withPivot('position')
            ->orderBy('pivot_position', 'asc');
    }

    /**
     * Get the admin record associated with the user.
     */
    public function attribute_family()
    {
        return $this->belongsTo('Vendorism\Attribute\Models\AttributeFamily');
    }


}
