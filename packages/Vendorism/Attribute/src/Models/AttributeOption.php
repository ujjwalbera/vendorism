<?php

namespace Vendorism\Attribute\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeOption extends Model
{
    public $timestamps = false;
    
    
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    public $translatedAttributes = ['label'];

    protected $fillable = ['admin_name', 'swatch_value', 'sort_order', 'attribute_id'];

    /**
     * Get the attribute that owns the attribute option.
     */
    public function attribute()
    {
        return $this->belongsTo('Vendorism\Attribute\Models\Attribute');
    }

    /**
     * Get image url for the swatch value url.
     */
    public function swatch_value_url()
    {
        if ($this->swatch_value && $this->attribute->swatch_type == 'image')
            return Storage::url($this->swatch_value);

        return;
    }

    /**
     * Get image url for the product image.
     */
    public function getSwatchValueUrlAttribute()
    {
        return $this->swatch_value_url();
    }
}
