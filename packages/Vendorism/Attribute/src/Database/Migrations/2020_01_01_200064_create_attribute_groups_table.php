<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('position');
            $table->string('name');
            $table->string('code');
            $table->bigInteger('attribute_family_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->boolean('is_user_defined')->default(true);
            $table->softDeletes();
            $table->unique(['attribute_family_id', 'code']);
            $table->foreign('attribute_family_id')->references('id')->on('attribute_families')->onDelete('cascade');
        });
        DB::update("ALTER TABLE attribute_groups AUTO_INCREMENT = 1000000000000009999;");

        Schema::create('attribute_group_mappings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('attribute_id')->unsigned();
            $table->bigInteger('attribute_group_id')->unsigned();
            $table->integer('position')->nullable();
            $table->unique(['attribute_id', 'attribute_group_id']);
            $table->boolean('status')->default(false);
            $table->softDeletes();
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->foreign('attribute_group_id')->references('id')->on('attribute_groups')->onDelete('cascade');
        });
        DB::update("ALTER TABLE attribute_group_mappings AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('attribute_group_mappings');
        Schema::dropIfExists('attribute_groups');


    }
}
