<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique();
	        $table->string('frontend_name')->nullable();
            $table->integer('sort_order')->nullable();
            $table->bigInteger('attribute_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });
        DB::update("ALTER TABLE attribute_options AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_options');
    }
}
