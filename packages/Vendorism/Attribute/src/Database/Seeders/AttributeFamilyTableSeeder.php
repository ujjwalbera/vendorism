<?php

namespace Vendorism\Attribute\Seeds;

use Illuminate\Database\Seeder;
use DB;

class AttributeFamilyTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('attribute_families')->delete();

        DB::table('attribute_families')->insert([
            ['id' => 1,'code' => 'grocery','name' => 'Grocery','status' => true,'is_user_defined' => false],
            ['id' => 2,'code' => 'beauty-hygiene','name' => 'Beauty & Hygiene','status' => true,'is_user_defined' => true],
            ['id' => 3,'code' => 'vegetable','name' => 'Vegetable','status' => true,'is_user_defined' => true],
            ['id' => 4,'code' => 'cleaning-household','name' => 'Cleaning & Household','status' => true,'is_user_defined' => true]
        ]);
    }
}
