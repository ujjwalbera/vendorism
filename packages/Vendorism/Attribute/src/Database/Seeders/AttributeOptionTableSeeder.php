<?php

namespace Vendorism\Attribute\Seeds;

use Illuminate\Database\Seeder;
use DB;

class AttributeOptionTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('attribute_options')->delete();

        DB::table('attribute_options')->insert([
            ['id' => 1, 'code' => 'Red',    'frontend_name' => 'Red',   'sort_order' => '1', 'attribute_id' => 10],
            ['id' => 2, 'code' => 'Green',  'frontend_name' => 'Green', 'sort_order' => '2', 'attribute_id' => 10],
            ['id' => 3, 'code' => 'Yellow', 'frontend_name' => 'Yellow','sort_order' => '3', 'attribute_id' => 10],
            ['id' => 4, 'code' => 'Black',  'frontend_name' => 'Black', 'sort_order' => '4', 'attribute_id' => 10],
            ['id' => 5, 'code' => 'White',  'frontend_name' => 'White', 'sort_order' => '5', 'attribute_id' => 10],
            
            ['id' => 6, 'code' => 'S',      'frontend_name' => 'S',     'sort_order' => '1', 'attribute_id' => 11],
            ['id' => 7, 'code' => 'M',      'frontend_name' => 'M',     'sort_order' => '2', 'attribute_id' => 11],
            ['id' => 8, 'code' => 'L',      'frontend_name' => 'L',     'sort_order' => '3', 'attribute_id' => 11],
            ['id' => 9, 'code' => 'XL',     'frontend_name' => 'XL',    'sort_order' => '4', 'attribute_id' => 11],

            ['id' => 10, 'code' => '0',     'frontend_name' => '0%',     'sort_order' => '1', 'attribute_id' => 4],
            ['id' => 11, 'code' => '5',     'frontend_name' => '5%',     'sort_order' => '1', 'attribute_id' => 4],
            ['id' => 12, 'code' => '18',    'frontend_name' => '18%',     'sort_order' => '1', 'attribute_id' => 4],
            ['id' => 13, 'code' => '28',    'frontend_name' => '28%',     'sort_order' => '1', 'attribute_id' => 4],
            ['id' => 14, 'code' => '32',    'frontend_name' => '32%',     'sort_order' => '1', 'attribute_id' => 4]
        ]);
    }
}
