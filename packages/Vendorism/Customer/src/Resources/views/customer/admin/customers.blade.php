@extends('layouts.admin.main')
@section('content')
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Customer listing</h3>
                <span class="float-right">
                    <!--<a href="" class="btn btn-block btn-outline-primary btn-sm">Add New</a>-->
                </span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <!--<th>Action</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->phone }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->username }}</td>
                        <td>
                            @if($customer->status == true)
                                <img src="{!! URL::asset('assets/admin/status/active.png') !!}" alt="Active"
                                     title="Active" class="statusbutton">
                            @else
                                <img src="{!! URL::asset('assets/admin/status/inactive.png') !!}" alt="In-Active"
                                     title="In-Active" class="statusbutton">
                            @endif
                        </td>
                        <!--<td>
                            <a href="{{ route('admin-customer-edit', ['customer' => $customer->id]) }}">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            /
                            <a href="{{ route('admin-customer-delete', ['customer' => $customer->id]) }}" style="color:#ff0000;">
                                <i class="fas fa-trash"></i> Delete
                            </a>
                        </td>-->
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th>Image</th>
                        <th>Position</th>
                        <th>Status</th>
                        <!--<th>Action</th>-->
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row (main row) -->
@endsection
