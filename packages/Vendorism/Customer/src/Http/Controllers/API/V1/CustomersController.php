<?php
namespace Vendorism\Customer\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
use Mail;

use Vendorism\Customer\Models\Customer;
use Vendorism\Customer\Models\Address;

use Laravel\Passport\HasApiTokens;
use App\Http\Controllers\ResponseObject;

class CustomersController extends Controller
{

    /**
     * RoleRepository object
     *
     * @var array
     */
    protected $role;


    protected $table = 'customers';


    protected $customerAddress = 'customer_address';

    public $successStatus = 200;

    public $response = null;

    //public $response = null;



    /**
     * Create a new controller instance.
     *
     * @param  \Vendorism\Customer\Repositories\RoleRepository  $role
     * @return void
     */
    public function __construct(/*Role $role*/)
    {
        Parent::__construct();
        $this->customers = 'customers';
        $this->customerProfiles = 'customer_address';
        //$this->resObj = new ResponseObject;
    }

    /*protected function guard()
    {
        return Auth::guard('customer-api');
    }*/

    public function api_login(Request $request) {
        $validator = Validator::make(
            $request->all('username', 'password'), 
            [
                'username' => 'required',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        };

        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'phone';

        $request->merge([
            $login_type => $request->input('username')
        ]);

        //dd($request->only($login_type, 'password'));

        if (Auth::guard('customer-api')->check($request->only($login_type, 'password'))) {
            $customer = Auth::guard('customer_api');
            $success['token'] = $customer->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success], 200);
        } else {
            return response()->json(['error'=>'Can not log in with the data provide.'], 401);
        }
    }

    public function api_login1(Request $request){
        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'phone';

        $request->merge([
            $login_type => $request->input('username')
        ]);
        //if (Auth::guard('customer-api')->attempt($credentials)) {
        //echo 'In api_login'; die;



        if (Auth::guard('customer_api')->check($request->only($login_type, 'password'))) {
            $customer = Auth::guard('customer_api');
            $success['token'] = $customer->createToken('VendorApp')->accessToken;
            return response()->json(['success' => $success], 200);
        } else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


    public function api_register(Request $request)
    {
        //echo 'In Registration'; die;
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'phone' => 'required|numeric|regex:/[0-9]{10}/|unique:' . $this->customers,
            'email' => 'required|email|unique:' . $this->customers,
            'password' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/',
            'password_confirmation' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/|same:password',
            'terms' => 'required'
        ];
        //print_r($request->json()->all()); die();
        $validator = Validator::make($request->json()->all(), $rules);
        if($validator->fails()){
            $messages = [];
            foreach ($validator->errors()->getMessages() as $item) {
                array_push($messages, $item);
            }

            $res['status'] = 'FAIL';
            $res['code'] = '400';
            $res['message'] = 'Customer Registration fail';
            $res['data'] = $messages;
            return response($res, 200);

        } else {
            //print_r($request->name); die();
            $customerData = $request->json()->all();
            //print_r($customerData); die();
            $customer = new Customer();
            $customer->name = $customerData['name'];
            $customer->phone = $customerData['phone'];
            $customer->email = $customerData['email'];
            $customer->password = Hash::make( $customerData['password'] );
            $customer->role_id = 1;
            $customer->provider = 'mobile-app';
            //$customer->remember_token = $request->_token;
            $customer->save();
            //$success['token'] =  $customer->createToken('VendorApp')->accessToken;
            //$success['name'] =  $customer->name;

            $res['status'] = 'OK';
            $res['code'] = '200';
            $res['message'] = 'Customer Registration success!';
            $res['data'] = $customer;
            return response($res, 200);
        }
        //$validator =  $request->validate($rules);
        /*if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $customer = new Customer();
        $customer->name = $request->input( 'name' );
        $customer->phone = $request->input( 'phone' );
        $customer->email = $request->input( 'email' );
        $customer->password = Hash::make( $request->input( 'password' ) );
        $customer->role_id = 1;
        $customer->provider = 'mobile-app';
        $customer->remember_token = $request->input( '_token' );
        $customer->save();
        $success['token'] =  $customer->createToken('VendorApp')-> accessToken;
        $success['name'] =  $customer->username;
        return response()->json(['success' => $success], $this->successStatus);*/
    }






    /**
     * @return void
     */
    public function doRegistration(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'phone' => 'required|numeric|regex:/[0-9]{10}/|unique:' . $this->table,
            'email' => 'required|email|unique:' . $this->table,
            'username' => 'required|min:8|max:12|unique:' . $this->table,
            'password' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/',
            'password_confirmation' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/|same:password',
            'terms' => 'required'
        ];
        $data = $request->validate($rules);
        $customer = new Customer();
        $customer->phone = $request->input( 'phone' );
        $customer->email = $request->input( 'email' );
        $customer->username = $request->input( 'username' );
        $customer->password = Hash::make( $request->input( 'password' ) );
        $customer->role_id = 1;
        $customer->remember_token = $request->input( '_token' );
        $customer->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('alert-success', 'Customer was successful registration!');
        return redirect()->back();
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function registration_confirmation(Request $request, $id)
    {
        $user = Admin::findOrFail($id);

        Mail::send('customer::emails.confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from('ushadigitalsevakendra@gmail.com', 'Vendorism');

            $m->to($user->email, $user->name)->subject('Your registration confirmation!');
        });
    }

    /**
     * @return void
     */
    public function doLogout(Request $request)
    {
        Auth::guard('customer-api')->logout();
        $request->session()->flash('alert-success', 'Vendor was successfully logged out!');
        return redirect()->route('customer-login');
    }



    /**
     * @return void
     */
    public function dashboard()
    {
        //$id = Auth::user()->id;
        //DB::connection()->enableQueryLog();
        //$admin = User::find($id)->admin()->get();
        //$queries = \DB::getQueryLog();
        return view('customer::dashboard');
    }


    /**
     * @return void
     */
    public function admin_customers()
    {
        $customers = Customer::get();
        return view('customer::admin.customers', compact('customers'));
    }


}
