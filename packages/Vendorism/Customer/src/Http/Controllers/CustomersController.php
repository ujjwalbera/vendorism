<?php
namespace Vendorism\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
use Mail;

use Vendorism\Customer\Models\Customer;
use Vendorism\Customer\Models\Address;

class CustomersController extends Controller
{

    /**
     * RoleRepository object
     *
     * @var array
     */
    protected $role;


    protected $table = 'customers';


    protected $customerAddress = 'customer_address';



    /**
     * Create a new controller instance.
     *
     * @param  \Vendorism\Customer\Repositories\RoleRepository  $role
     * @return void
     */
    public function __construct(/*Role $role*/)
    {
        Parent::__construct();
        $this->admins = 'customers';
        $this->adminProfiles = 'customer_address';
    }

    /**
     * @return void
     */
    public function login()
    {
        if (Auth::guard('customer')->check()) {
            return redirect()->route('customer-dashboard');
        }
        return view('customer::login');
    }



    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Customer\Repositories\RoleRepository  $role
     * @return void
     */
    public function doLogin(Request $request)
    {
        //dd($request);
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $data = $request->validate($rules);

        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'phone';

        $request->merge([
            $login_type => $request->input('username')
        ]);

        if (Auth::guard('customer')->attempt($request->only($login_type, 'password'))) {
            //return redirect()->intended($this->redirectPath());
            $request->session()->flash('alert-success', 'Customer was successful login!');
            return redirect()->route('customer-dashboard');
        } else {
            $request->session()->flash('alert-warning', 'Invalid Credentials , Please try again.');
            return redirect()->route('customer-login');
        }


    }



    /**
     * @return void
     */
    public function forgot()
    {

    }



    /**
     * @return void
     */
    public function registration()
    {
        if (Auth::guard('customer')->check()) {
            return redirect()->route('customer-dashboard');
        }
        return view('customer::registration');
    }



    /**
     * @return void
     */
    public function doRegistration(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'phone' => 'required|numeric|regex:/[0-9]{10}/|unique:' . $this->table,
            'email' => 'required|email|unique:' . $this->table,
            'password' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/',
            'password_confirmation' => 'required|min:8|max:15|regex:/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=]).*$/|same:password',
            'terms' => 'required'
        ];
        $data = $request->validate($rules);
        $customer = new Customer();
        $customer->phone = $request->input( 'phone' );
        $customer->email = $request->input( 'email' );
        $customer->password = Hash::make( $request->input( 'password' ) );
        $customer->role_id = 1;
        $customer->remember_token = $request->input( '_token' );
        $customer->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('alert-success', 'Customer was successful registration!');
        return redirect()->back();
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function registration_confirmation(Request $request, $id)
    {
        $user = Admin::findOrFail($id);

        Mail::send('customer::emails.confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from('ushadigitalsevakendra@gmail.com', 'Vendorism');

            $m->to($user->email, $user->name)->subject('Your registration confirmation!');
        });
    }

    /**
     * @return void
     */
    public function doLogout(Request $request)
    {
        Auth::guard('customer')->logout();
        $request->session()->flash('alert-success', 'Vendor was successfully logged out!');
        return redirect()->route('customer-login');
    }



    /**
     * @return void
     */
    public function dashboard()
    {
        //$id = Auth::user()->id;
        //DB::connection()->enableQueryLog();
        //$admin = User::find($id)->admin()->get();
        //$queries = \DB::getQueryLog();
        return view('customer::dashboard');
    }


    /**
     * @return void
     */
    public function admin_customers()
    {
        $customers = Customer::get();
        return view('customer::admin.customers', compact('customers'));
    }


}
