<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('latitude', 10)->unique();
            $table->string('longitude', 10)->unique();
            $table->string('city', 50)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 15)->nullable();
            $table->boolean('confirmed');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes(0);
            $table->foreign('customer_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->index(['address', 'city', 'state']);
        });
        DB::update("ALTER TABLE customer_address AUTO_INCREMENT = 1000000000000009999;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_address');
    }
}
