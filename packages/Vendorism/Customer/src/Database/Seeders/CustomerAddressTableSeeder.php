<?php

namespace Vendorism\Customer\Seeds;

use Illuminate\Database\Seeder;
use DB;

class CustomerAddressTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('customer_address')->delete();

        DB::table('customer_address')->insert([
            [
                'id' => 1,
                'customer_id' => 1,
                'address' => 'Bonhijli, Khanakul, Hooghly',
                'latitude' => '22.634200',
                'longitude' => '87.842914',
                'city' => 'Hooghly',
                'state' => 'West Bengal',
                'country' => 'India',
                'confirmed' => true,
                'status' => true,
                'created_at' => '2020-01-06 07:29:44',
                'updated_at' => '2020-01-06 07:29:44'
            ]

        ]);
    }
}
