<?php

namespace Vendorism\Customer\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;

class CustomerTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('customers')->delete();

        DB::table('customers')->insert([
            [
                'id' => 1,
                'name' => 'Ujjwal Bera',
                'phone' => '7319473777',
                'email' => 'ujjwal.bera@gmail.com',
                'email_verified_at' => '2020-01-06 07:32:44',
                'email_verify_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'phone_verified_at' => '2020-01-06 07:32:44',
                'phone_verify_token' => '123456',
                'password' => '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC',
                'role_id' => 1,
                'verified' => true,
                'status' => true,
                'remember_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'created_at' => '2020-01-06 07:29:44',
                'updated_at' => '2020-01-06 07:29:44'
            ]
        ]);
    }
}
