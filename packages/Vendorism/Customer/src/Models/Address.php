<?php

namespace Vendorism\customer\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Address extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';

    protected $table = 'customer_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'email', 'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];


    /**
    * Get the user record associated with the admin.
    */
    public function customer_address()
    {
        return $this->belongsTo('Vendorism\Customer\Models\Customer', 'customer_id');
    }
}
