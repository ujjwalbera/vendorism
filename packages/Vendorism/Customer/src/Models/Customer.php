<?php

namespace Vendorism\customer\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Auth;



class customer extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';

    protected $table = 'customers';

    //protected $guard = 'customer';

    protected $guarded = ['id', 'provider', 'provider_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email_verified_at', 'email_verify_token', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'id' => 'string'
    ];


    /**
     * Get the admin record associated with the user.
     */
    public function address()
    {
        return $this->hasOne('Vendorism\Customer\Models\Address');
    }

    /**
     * Roll API Key
     */
    /*public function rollApiKey(){
        do{
            $this->api_token = str_random(60);
        }while($this->where('api_token', $this->api_token)->exists());
        $this->save();
    }

    protected function guard()
    {
        return Auth::guard('customer-api');
    }*/
}
