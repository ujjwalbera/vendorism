<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () {

    Route::get('/', function () {
        return view('customer::home');
    })->name('customer-home');

    Route::get('customer/registration', '\Vendorism\Customer\Http\Controllers\CustomersController@registration')->name('customer-registration');
    Route::post('customer/registration', '\Vendorism\Customer\Http\Controllers\CustomersController@doRegistration')->name('customer-doRegistration');

    Route::get('customer/login', '\Vendorism\Customer\Http\Controllers\CustomersController@login')->name('customer-login');
    Route::get('vendor', '\Vendorism\Customer\Http\Controllers\CustomersController@login')->name('customer-login');
    Route::post('customer/login', '\Vendorism\Customer\Http\Controllers\CustomersController@doLogin')->name('customer-doLogin');

    Route::get('customer/logout', '\Vendorism\Customer\Http\Controllers\CustomersController@doLogout')->name('customer-logout');

    /*Route::get('customer/forgot', function () {
        return view('customer::forgot');
    })->name('customer-forgot');*/

    Route::post('customer/forgot', 'Vendorism\Customer\Http\Controllers\CustomersController@doforgot')->name('customer-doforgot')->name('customer-forgot-set');

    /*Route::get('/admin/registration', function () {
        return view('admin::registration');
    })->name('admin-registration');

    Route::get('/admin/dashboard', function () {
        return view('admin::dashboard');
    })->name('admin-dashboard');*/

    Route::get('customer/dashboard', 'Vendorism\Customer\Http\Controllers\CustomersController@dashboard')->name('customer-dashboard');

    Route::get('admin/customers', 'Vendorism\Customer\Http\Controllers\CustomersController@admin_customers')->middleware('admin_loggedin')->name('admin-customers');

    Route::get('admin/customer/edit/{customer}', 'Vendorism\Customer\Http\Controllers\CustomersController@admin_customer_edit')->middleware('admin_loggedin')->name('admin-customer-edit');

    Route::put('admin/customer/update/{customer}', 'Vendorism\Customer\Http\Controllers\CustomersController@admin_customer_update')->middleware('admin_loggedin')->name('admin-customer-update');

    Route::post('admin/customer/delete/{customer}', 'Vendorism\Customer\Http\Controllers\CustomersController@admin_customer_delete')->middleware('admin_loggedin')->name('admin-customer-delete');





});


