<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('api/v1/customer/login', '\Vendorism\Customer\Http\Controllers\API\V1\CustomersController@api_login')->name('api_customer_login');//Updated
Route::post('api/v1/customer/register', '\Vendorism\Customer\Http\Controllers\API\V1\CustomersController@api_register')->name('api_customer_register');
/*
Route::group(['middleware' => 'auth:customer-api'], function () {
    Route::group(['prefix' => 'api/v1'], function ($apiv1) {
        $apiv1->post('/customer/login', '\Vendorism\Customer\Http\Controllers\API\V1\CustomersController@api_login');
        $apiv1->post('/customer/register', '\Vendorism\Customer\Http\Controllers\API\V1\CustomersController@api_register');
    });
});
*/
