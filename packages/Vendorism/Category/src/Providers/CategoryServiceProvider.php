<?php

namespace Vendorism\Category\Providers;

use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
        $this->loadViewsFrom(__DIR__.'/../Resources/views/category', 'category');
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/category'),
            __DIR__.'/../resources/views' => base_path('resources/views/Vendorism/role'),
        ]);*/
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
