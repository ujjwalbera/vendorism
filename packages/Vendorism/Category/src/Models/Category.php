<?php

namespace Vendorism\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;



class Category extends Model
{
    public $translatedAttributes = ['name', 'description', 'slug', 'meta_title', 'meta_description', 'meta_keywords'];

    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Indicates key type.
     *
     * @var string
     */
    public $keyType = 'string';
    //protected $fillable = ['position', 'status', 'display_mode', 'parent_id'];

	public $fillable = ['name','parent_id'];

    protected $guarded = array('_lft', '_rgt', 'name', 'created_at', 'updated_at');


    /**
     * Get image url for the category image.
     */
    public function image_url()
    {
        if (! $this->image)
            return;

        return Storage::url($this->image);
    }

    public function subcategories() {
	   return $this->hasMany('Vendorism\Category\Models\Category', 'parent_id','id');
    }

	public function childs() {
        return $this->hasMany('Vendorism\Category\Models\Category', 'id','parent_id');
    }

	public function parent() {
		return $this->belongsTo('Vendorism\Category\Models\Category', 'parent_id','id');
	}

    /**
     * Get image url for the category image.
     */
    public function getImageUrlAttribute()
    {
        return $this->image_url();
    }
}
