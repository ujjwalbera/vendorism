<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {

    Route::get('admin/categories', 'Vendorism\Category\Http\Controllers\CategoryController@admin_list')->middleware('admin_loggedin')->name('admin-categories');
    Route::get('admin/category/add', 'Vendorism\Category\Http\Controllers\CategoryController@admin_create')->middleware('admin_loggedin')->name('admin-category-add');
    Route::post('admin/category/store', 'Vendorism\Category\Http\Controllers\CategoryController@admin_store')->middleware('admin_loggedin')->name('admin-category-store');
    Route::get('admin/category/edit/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@admin_edit')->middleware('admin_loggedin')->name('admin-category-edit');
    Route::post('admin/category/update/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@admin_update')->middleware('admin_loggedin')->name('admin-category-update');
    Route::get('admin/category/delete/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@admin_destroy')->middleware('admin_loggedin')->name('admin-category-delete');

    Route::get('vendor/categories', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_list')->middleware('vendor_loggedin')->name('vendor-categories');
    Route::get('vendor/category/add', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_create')->middleware('vendor_loggedin')->name('vendor-category-add');
    Route::post('vendor/category/store', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_store')->middleware('vendor_loggedin')->name('vendor-category-store');
    Route::get('vendor/category/edit/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_edit')->middleware('vendor_loggedin')->name('vendor-category-edit');
    Route::post('vendor/category/update/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_update')->middleware('vendor_loggedin')->name('vendor-category-update');
    Route::get('vendor/category/delete/{category_id}', 'Vendorism\Category\Http\Controllers\CategoryController@vendor_destroy')->middleware('vendor_loggedin')->name('vendor-category-delete');

});


