@foreach($subcategories as $subcatoption)
    <option value="{{ $subcatoption->id }}" @if(isset($current->id) && ($current->id == $subcatoption->id)) {{ 'disabled' }} @endif @if(isset($current->parent_id) &&
    ($current->parent_id == $subcatoption->id)) {{ 'selected' }} @endif @if($disabled) {{ 'disabled' }} @endif>{{ $child }}{{$subcatoption->name }}</option>

    @php $disabled = false; @endphp
    @if(($current != null) && ($current->id == $subcatoption->id))
        $disabled = true;
    @endif

    @if(count($subcatoption->subcategories))
        @include('subcatoptions', ['subcategories' => $subcatoption->subcategories, 'child' => '-'.$child, 'current'
         => $current, 'disabled' => $disabled])
    @endif
@endforeach
