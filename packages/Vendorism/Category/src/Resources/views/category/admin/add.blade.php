@extends('layouts.admin.main')
@section('content')
<!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Product Category Add</h3>
              <span class="float-right">
                <a href="<?php echo route('admin-categories'); ?>" class="btn btn-block btn-outline-primary
                btn-sm">Back</a>
            </span>
              <div class="flash-message">
                  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                      @if(Session::has('alert-' . $msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endif
                  @endforeach
              </div>
          </div>
            @if (Session::has('message'))
                <div class="alert alert-warning">{{ Session::get('message') }}</div>
            @endif
            <form role="form" action="{{action('\Vendorism\Category\Http\Controllers\CategoryController@admin_store')}}" method="post">
                <!-- /.card-header -->
                @csrf
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Choose Parent Category</label>
                    <select name="parent" id="parent"  class="form-control">
                        <option value="0">No Parent</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                             @if(count($category->subcategories))
                                @include('category::subcatoptions', ['subcategories' => $category->subcategories, 'child' =>
                                '-', 'current' => null, 'disabled' => null])
                            @endif
                        @endforeach
                    </select>
                    @error('parent') <div class="error"> {{ $message }} </div> @enderror
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Status</label>
                    <select name="status" id="status"  class="form-control">
                    <option selected="selected" value="1">Active</option>
                    <option value="0">In-Active</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Category name">
                    @error('name') <div class="error"> {{ $message }} </div> @enderror
                </div>
                <!-- /.form-group -->
                  <div class="form-group">
                      <label for="customFile">Category Image</label>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input" id="image" name="image">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                      </div>
                  </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
              <div class="card-footer">
                  <button type="submit" class="btn btn-success">Save</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
              </div>
          </div>
            </form>
          <!-- /.card-body -->
          <div class="card-footer">
            <!--Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information
              about
            the plugin.-->
          </div>

        </div>
        <!-- /.card -->
@endsection
