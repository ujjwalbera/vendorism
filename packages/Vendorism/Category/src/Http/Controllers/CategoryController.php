<?php

namespace Vendorism\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use DB;


use Vendorism\Category\Models\Category;

/**
 * Catalog category controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CategoryController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $category;

    /**
     * AttributeRepository object
     *
     * @var array
     */
    protected $attribute;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Category\Repositories\CategoryRepository       $category
     * @param  use Webkul\Attribute\Repositories\AttributeRepository  $attribute
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Display a listing of the resource in admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_list()
    {
        //DB::enableQueryLog();
        $categories = Category::with('parent')->orderBy('parent_id', 'ASC')->orderBy('position', 'ASC')->get();
        //$query = DB::getQueryLog();
        //dd($query);
        //dd($categories->toArray());
        return view('category::admin.categories', compact('categories'));
    }

    /**
     * Display a listing of the resource in admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendor_list()
    {
        //DB::enableQueryLog();
        $categories = Category::with('parent')->orderBy('position', 'ASC')->orderBy('parent_id', 'ASC')->get();
        //$query = DB::getQueryLog();
        //dd($query);
        //dd($categories->toArray());
        return view('category::vendor.categories', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_create()
    {

        $categories = Category::where('parent_id', '=', null)->get();
        //dd($categories->toArray());
		return view('category::admin.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     **/
    public function admin_store(Request $request)
    {
        Event::dispatch('category.create.before');
        $data = $request->validate([
            'name' => 'required|regex:/^[A-Za-z0-9 ]+$/',
            'image.*' => 'mimes:jpeg,jpg,bmp,png'
        ]);

        $category = new Category();
        $category->name = $request->input( 'name' );
        $category->status = $request->input( 'status' );
        $category->parent_id = $request->input( 'parent' );
        $category->save();
        $request->session()->flash('alert-success', 'Category was successful added!');
        return redirect()->back();
    }

    public function admin_edit($id){
        $current = Category::where('id', $id)->first();
        //dd($category->toArray());
        $categories = Category::where('parent_id', '=', null)->get();
        return view('category::admin.edit',compact('current', 'categories'));
    }

    public function admin_update(Request $request, $id) {
        Event::dispatch('category.update.before');
        $data = $request->validate([
            'name' => 'required|regex:/^[A-Za-z0-9 ]+$/',
            'image.*' => 'mimes:jpeg,jpg,bmp,png'
        ]);

        $category = Category::where('id', $id)->first();
        $category->name = $request->input( 'name' );
        $category->status = $request->input( 'status' );
        $category->parent_id = $request->input( 'parent' );
        $category->save();
        $request->session()->flash('alert-success', 'Category was successful updated!');
        return redirect()->route('admin-categories');
    }



    public function admin_destroy( Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $subcategories = Category::where('parent_id', $id)->get();
        if (count($subcategories)){
            $request->session()->flash('alert-info', 'Category have child categories. Please delete child categories before delete the parent category.');
        } else {
            try {
                $category->delete();
                if ($category->trashed()) {
                    //some logic
                }
                $request->session()->flash('alert-success', 'Category was successful deleted!');
            } catch(\Exception $e) {
                $request->session()->flash('alert-error', 'Unable to delete the category. Please try again!');
            }
        }
        return redirect()->route('admin-categories');
    }





    /**
     * Remove the specified resources from database
     *
     * @return response \Illuminate\Http\Response
     */
    public function massDestroy() {
        $suppressFlash = false;

        if (request()->isMethod('delete') || request()->isMethod('post')) {
            $indexes = explode(',', request()->input('indexes'));

            foreach ($indexes as $key => $value) {
                try {
                    Event::fire('catalog.category.delete.before', $value);

                    $this->category->delete($value);

                    Event::fire('catalog.category.delete.after', $value);
                } catch(\Exception $e) {
                    $suppressFlash = true;

                    continue;
                }
            }

            if (! $suppressFlash)
                session()->flash('success', trans('admin::app.datagrid.mass-ops.delete-success'));
            else
                session()->flash('info', trans('admin::app.datagrid.mass-ops.partial-action', ['resource' => 'Attribute Family']));

            return redirect()->back();
        } else {
            session()->flash('error', trans('admin::app.datagrid.mass-ops.method-error'));

            return redirect()->back();
        }
    }
}
