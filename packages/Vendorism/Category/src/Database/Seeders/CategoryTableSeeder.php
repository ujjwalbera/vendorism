<?php

namespace Vendorism\Category\Seeds;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->delete();
        $now = Carbon::now();
        DB::table('categories')->insert([
            ['id' => 1,'name' => 'Grocery','position' => '1','image' => NULL,'status' => true,'_lft' => 1,'_rgt' => 4, 'parent_id' => NULL, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2,'name' => 'Foodgrains','position' => '2','image' => NULL,'status' => true,'_lft' => 1,'_rgt' => 4,'parent_id' => 1, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3,'name' => 'Dry Fruits','position' => '3','image' => NULL,'status' => true,'_lft' => 1,'_rgt' => 4,'parent_id' => 2, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 4,'name' => 'Dals & Pulses','position' => '4','image' => NULL,'status' => true,'_lft' => 1,'_rgt' => 4,'parent_id' => 2, 'created_at' => $now, 'updated_at' => $now]
        ]);
    }
}
