-- phpMyAdmin SQL Dump
-- version 4.9.2deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2020 at 12:33 AM
-- Server version: 8.0.19-0ubuntu2
-- PHP Version: 7.3.11-0ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vendorism`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `email_verified_at`, `email_verify_token`, `username`, `password`, `role_id`, `verified`, `status`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270162b', 'ushadigitalsevakendra@gmail.com', '2020-01-06 02:02:44', '1', 'ushadigital', '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC', 1, 1, 1, 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt', NULL, '2020-01-06 01:59:44', '2020-01-06 01:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_profiles`
--

CREATE TABLE `admin_profiles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_profiles`
--

INSERT INTO `admin_profiles` (`id`, `admin_id`, `name`, `address`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
('84f08cfe-fc4c-4b6f-88c3-8f87ca1aa71c', 'f607d2a7-a744-40ec-b4be-1250a270162b', 'Ujjwal Bera', 'Bonhijli, Khanakul, Hooghly', 0, '2020-01-06 01:59:44', '2020-01-06 01:59:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int DEFAULT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_unique` tinyint(1) NOT NULL DEFAULT '0',
  `is_filterable` tinyint(1) NOT NULL DEFAULT '0',
  `is_configurable` tinyint(1) NOT NULL DEFAULT '0',
  `is_user_defined` tinyint(1) NOT NULL DEFAULT '1',
  `is_visible_on_front` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `code`, `admin_name`, `type`, `validation`, `position`, `is_required`, `is_unique`, `is_filterable`, `is_configurable`, `is_user_defined`, `is_visible_on_front`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270163b', 'sku', 'SKU', 'text', NULL, 1, 1, 1, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270164b', 'item_name', 'Name', 'text', NULL, 2, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270165b', 'url_key', 'URL Key', 'text', NULL, 3, 1, 1, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270166b', 'category_id', 'Category', 'category', NULL, 4, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270167b', 'new', 'New', 'boolean', NULL, 5, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270168b', 'featured', 'Featured', 'boolean', NULL, 6, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270169b', 'visible_individually', 'Visible Individually', 'boolean', NULL, 7, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270170b', 'status', 'Status', 'boolean', NULL, 8, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270171b', 'color', 'Color', 'select', NULL, 9, 0, 0, 1, 1, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270172b', 'size', 'Size', 'select', NULL, 10, 0, 0, 1, 1, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270173b', 'brand', 'Brand', 'select', NULL, 11, 0, 0, 1, 0, 0, 1, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270174b', 'short_description', 'Short Description', 'textarea', NULL, 12, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270175b', 'description', 'Description', 'textarea', NULL, 13, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270176b', 'price', 'Price', 'price', 'decimal', 14, 1, 0, 1, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270177b', 'cost', 'Cost', 'price', 'decimal', 15, 0, 0, 0, 0, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270178b', 'special_price', 'Special Price', 'price', 'decimal', 16, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270179b', 'special_price_from', 'Special Price From', 'date', NULL, 17, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270180b', 'special_price_to', 'Special Price To', 'date', NULL, 18, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270181b', 'tax_category_id', 'Tax\n            Category', 'select', NULL, 4, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270182b', 'meta_title', 'Meta Title', 'textarea', NULL, 19, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270183b', 'meta_keywords', 'Meta Keywords', 'textarea', NULL, 20, 0, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270184b', 'meta_description', 'Meta Description', 'textarea', NULL, 21, 0, 0, 0, 0, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270185b', 'width', 'Width', 'text', 'decimal', 22, 0, 0, 0, 0, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270186b', 'height', 'Height', 'text', 'decimal', 23, 0, 0, 0, 0, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270187b', 'depth', 'Depth', 'text', 'decimal', 24, 0, 0, 0, 0, 1, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02'),
('f607d2a7-a744-40ec-b4be-1250a270188b', 'weight', 'Weight', 'text', 'decimal', 25, 1, 0, 0, 0, 0, 0, 1, NULL, '2020-02-06 13:33:02', '2020-02-06 13:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_families`
--

CREATE TABLE `attribute_families` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_user_defined` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_families`
--

INSERT INTO `attribute_families` (`id`, `code`, `name`, `status`, `is_user_defined`, `deleted_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270197b', 'grocery', 'Grocery', 1, 0, NULL),
('f607d2a7-a744-40ec-b4be-1250a270198b', 'beauty-hygiene', 'Beauty & Hygiene', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270199b', 'vegetable', 'Vegetable', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270200b', 'cleaning-household', 'Cleaning & Household', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT '1',
  `attribute_family_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `position`, `is_user_defined`, `attribute_family_id`, `deleted_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270201b', 'General', 1, 1, 'f607d2a7-a744-40ec-b4be-1250a270197b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270202b', 'Description', 2, 1, 'f607d2a7-a744-40ec-b4be-1250a270197b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270203b', 'Price', 3, 1, 'f607d2a7-a744-40ec-b4be-1250a270197b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270204b', 'Meta Description', 4, 1, 'f607d2a7-a744-40ec-b4be-1250a270197b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270205b', 'Shipping', 5, 1, 'f607d2a7-a744-40ec-b4be-1250a270197b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270206b', 'General', 1, 1, 'f607d2a7-a744-40ec-b4be-1250a270198b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270207b', 'Description', 2, 1, 'f607d2a7-a744-40ec-b4be-1250a270198b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270208b', 'Price', 3, 1, 'f607d2a7-a744-40ec-b4be-1250a270198b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270209b', 'Meta Description', 4, 1, 'f607d2a7-a744-40ec-b4be-1250a270198b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270210b', 'Shipping', 5, 1, 'f607d2a7-a744-40ec-b4be-1250a270198b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270211b', 'General', 1, 1, 'f607d2a7-a744-40ec-b4be-1250a270199b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270212b', 'Description', 2, 1, 'f607d2a7-a744-40ec-b4be-1250a270199b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270213b', 'Price', 3, 1, 'f607d2a7-a744-40ec-b4be-1250a270199b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270214b', 'Meta Description', 4, 1, 'f607d2a7-a744-40ec-b4be-1250a270199b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270215b', 'Shipping', 5, 1, 'f607d2a7-a744-40ec-b4be-1250a270199b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270216b', 'General', 1, 1, 'f607d2a7-a744-40ec-b4be-1250a270200b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270217b', 'Description', 2, 1, 'f607d2a7-a744-40ec-b4be-1250a270200b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270218b', 'Price', 3, 1, 'f607d2a7-a744-40ec-b4be-1250a270200b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270219b', 'Meta Description', 4, 1, 'f607d2a7-a744-40ec-b4be-1250a270200b', NULL),
('f607d2a7-a744-40ec-b4be-1250a270220b', 'Shipping', 5, 1, 'f607d2a7-a744-40ec-b4be-1250a270200b', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group_mappings`
--

CREATE TABLE `attribute_group_mappings` (
  `attribute_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_group_mappings`
--

INSERT INTO `attribute_group_mappings` (`attribute_id`, `attribute_group_id`, `position`, `status`, `deleted_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270163b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270163b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270163b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270163b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270164b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270164b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270164b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270164b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270165b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270165b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270165b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270165b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270166b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270166b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270166b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270166b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270167b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270167b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270167b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270167b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270168b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270168b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270168b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270168b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270169b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 7, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270169b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 7, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270169b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 7, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270169b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 7, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270170b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 8, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270170b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 8, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270170b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 8, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270170b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 8, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270171b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 9, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270171b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 9, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270171b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 9, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270171b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 9, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270172b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 10, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270172b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 10, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270172b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 10, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270172b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 10, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270173b', 'f607d2a7-a744-40ec-b4be-1250a270201b', 11, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270173b', 'f607d2a7-a744-40ec-b4be-1250a270206b', 11, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270173b', 'f607d2a7-a744-40ec-b4be-1250a270211b', 11, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270173b', 'f607d2a7-a744-40ec-b4be-1250a270216b', 11, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270174b', 'f607d2a7-a744-40ec-b4be-1250a270202b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270174b', 'f607d2a7-a744-40ec-b4be-1250a270207b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270174b', 'f607d2a7-a744-40ec-b4be-1250a270212b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270174b', 'f607d2a7-a744-40ec-b4be-1250a270217b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270175b', 'f607d2a7-a744-40ec-b4be-1250a270202b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270175b', 'f607d2a7-a744-40ec-b4be-1250a270207b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270175b', 'f607d2a7-a744-40ec-b4be-1250a270212b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270175b', 'f607d2a7-a744-40ec-b4be-1250a270217b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270176b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270176b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270176b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270176b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270177b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270177b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270177b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270177b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270178b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270178b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270178b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270178b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270179b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270179b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270179b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270179b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270180b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270180b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270180b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270180b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 5, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270181b', 'f607d2a7-a744-40ec-b4be-1250a270203b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270181b', 'f607d2a7-a744-40ec-b4be-1250a270208b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270181b', 'f607d2a7-a744-40ec-b4be-1250a270213b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270181b', 'f607d2a7-a744-40ec-b4be-1250a270218b', 6, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270182b', 'f607d2a7-a744-40ec-b4be-1250a270204b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270182b', 'f607d2a7-a744-40ec-b4be-1250a270209b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270182b', 'f607d2a7-a744-40ec-b4be-1250a270214b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270182b', 'f607d2a7-a744-40ec-b4be-1250a270219b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270183b', 'f607d2a7-a744-40ec-b4be-1250a270204b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270183b', 'f607d2a7-a744-40ec-b4be-1250a270209b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270183b', 'f607d2a7-a744-40ec-b4be-1250a270214b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270183b', 'f607d2a7-a744-40ec-b4be-1250a270219b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270184b', 'f607d2a7-a744-40ec-b4be-1250a270204b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270184b', 'f607d2a7-a744-40ec-b4be-1250a270209b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270184b', 'f607d2a7-a744-40ec-b4be-1250a270214b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270184b', 'f607d2a7-a744-40ec-b4be-1250a270219b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270185b', 'f607d2a7-a744-40ec-b4be-1250a270205b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270185b', 'f607d2a7-a744-40ec-b4be-1250a270210b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270185b', 'f607d2a7-a744-40ec-b4be-1250a270215b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270185b', 'f607d2a7-a744-40ec-b4be-1250a270220b', 1, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270186b', 'f607d2a7-a744-40ec-b4be-1250a270205b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270186b', 'f607d2a7-a744-40ec-b4be-1250a270210b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270186b', 'f607d2a7-a744-40ec-b4be-1250a270215b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270186b', 'f607d2a7-a744-40ec-b4be-1250a270220b', 2, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270187b', 'f607d2a7-a744-40ec-b4be-1250a270205b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270187b', 'f607d2a7-a744-40ec-b4be-1250a270210b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270187b', 'f607d2a7-a744-40ec-b4be-1250a270215b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270187b', 'f607d2a7-a744-40ec-b4be-1250a270220b', 3, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270188b', 'f607d2a7-a744-40ec-b4be-1250a270205b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270188b', 'f607d2a7-a744-40ec-b4be-1250a270210b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270188b', 'f607d2a7-a744-40ec-b4be-1250a270215b', 4, 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270188b', 'f607d2a7-a744-40ec-b4be-1250a270220b', 4, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frontend_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int DEFAULT NULL,
  `attribute_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_options`
--

INSERT INTO `attribute_options` (`id`, `code`, `frontend_name`, `sort_order`, `attribute_id`, `status`, `deleted_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270188b', 'Red', 'Red', 1, 'f607d2a7-a744-40ec-b4be-1250a270171b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270189b', 'Green', 'Green', 2, 'f607d2a7-a744-40ec-b4be-1250a270171b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270190b', 'Yellow', 'Yellow', 3, 'f607d2a7-a744-40ec-b4be-1250a270171b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270191b', 'Black', 'Black', 4, 'f607d2a7-a744-40ec-b4be-1250a270171b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270192b', 'White', 'White', 5, 'f607d2a7-a744-40ec-b4be-1250a270171b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270193b', 'S', 'S', 1, 'f607d2a7-a744-40ec-b4be-1250a270172b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270194b', 'M', 'M', 2, 'f607d2a7-a744-40ec-b4be-1250a270172b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270195b', 'L', 'L', 3, 'f607d2a7-a744-40ec-b4be-1250a270172b', 1, NULL),
('f607d2a7-a744-40ec-b4be-1250a270196b', 'XL', 'XL', 4, 'f607d2a7-a744-40ec-b4be-1250a270172b', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int DEFAULT NULL,
  `_rgt` int DEFAULT NULL,
  `display_mode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'products_and_description',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `position`, `image`, `parent_id`, `_lft`, `_rgt`, `display_mode`, `status`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270221b', 'Grocery', 1, NULL, NULL, 1, 14, 'products_and_description', 1, '2020-02-06 13:33:03', '2020-02-06 13:33:03'),
('f607d2a7-a744-40ec-b4be-1250a270222b', 'Foodgrains', 2, NULL, 'f607d2a7-a744-40ec-b4be-1250a270221b', 1, 14, 'products_and_description', 1, '2020-02-06 13:33:03', '2020-02-06 13:33:03'),
('f607d2a7-a744-40ec-b4be-1250a270223b', 'Dry Fruits', 3, NULL, 'f607d2a7-a744-40ec-b4be-1250a270222b', 1, 14, 'products_and_description', 1, '2020-02-06 13:33:03', '2020-02-06 13:33:03'),
('f607d2a7-a744-40ec-b4be-1250a270224b', 'Dals & Pulses', 4, NULL, 'f607d2a7-a744-40ec-b4be-1250a270222b', 1, 14, 'products_and_description', 1, '2020-02-06 13:33:03', '2020-02-06 13:33:03');

-- --------------------------------------------------------

--
-- Table structure for table `category_filterable_attributes`
--

CREATE TABLE `category_filterable_attributes` (
  `category_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_verified_at` timestamp NULL DEFAULT NULL,
  `phone_verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `email_verified_at`, `email_verify_token`, `phone_verified_at`, `phone_verify_token`, `username`, `password`, `role_id`, `provider`, `provider_id`, `verified`, `status`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270163b', 'Ujjwal Bera', 'ujjwal.bera@gmail.com', '7319473777', '2020-01-06 02:02:44', 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt', '2020-01-06 02:02:44', '123456', 'ujjwalbera', '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC', 1, 'web', '0', 1, 1, 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt', NULL, '2020-01-06 01:59:44', '2020-01-06 01:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

CREATE TABLE `customer_address` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `0` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_address`
--

INSERT INTO `customer_address` (`id`, `customer_id`, `address`, `latitude`, `longitude`, `city`, `state`, `country`, `confirmed`, `status`, `created_at`, `updated_at`, `0`) VALUES
('84f08cfe-fc4c-4b6f-88c3-8f87ca1aa72c', 'f607d2a7-a744-40ec-b4be-1250a270163b', 'Bonhijli, Khanakul, Hooghly', '22.634200', '87.842914', 'Hooghly', 'West Bengal', 'India', 1, 1, '2020-01-06 01:59:44', '2020-01-06 01:59:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_01_200001_create_admins_table', 1),
(5, '2020_01_01_200002_create_admin_profiles_table', 1),
(6, '2020_01_01_200011_create_vendors_table', 1),
(7, '2020_01_01_200012_create_vendor_profiles_table', 1),
(8, '2020_01_01_200013_create_customers_table', 1),
(9, '2020_01_01_200014_create_customer_address_table', 1),
(10, '2020_01_01_200061_create_attributes_table', 1),
(11, '2020_01_01_200063_create_attribute_families_table', 1),
(12, '2020_01_01_200064_create_attribute_groups_table', 1),
(13, '2020_01_01_200065_create_attribute_options_table', 1),
(14, '2020_01_01_200071_create_categories_table', 1),
(15, '2020_01_01_200072_create_category_filterable_attributes_table', 1),
(16, '2020_01_01_200081_create_products_table', 1),
(17, '2020_01_01_200082_create_product_attribute_values_table', 1),
(18, '2020_01_01_200083_create_product_reviews_table', 1),
(19, '2020_01_01_200084_create_product_images_table', 1),
(20, '2020_01_01_200085_create_product_inventories_table', 1),
(21, '2020_01_01_200087_create_product_ordered_inventories_table', 1),
(22, '2020_01_17_041909_create_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_family_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `vendor_id`, `parent_id`, `attribute_family_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270225b', 'Masoor Dal', 'f607d2a7-a744-40ec-b4be-1250a270163b', NULL, 'f607d2a7-a744-40ec-b4be-1250a270197b', 1, NULL, '2020-02-06 13:33:04', '2020-02-06 13:33:04'),
('f607d2a7-a744-40ec-b4be-1250a270226b', 'Moong Dal', 'f607d2a7-a744-40ec-b4be-1250a270163b', NULL, 'f607d2a7-a744-40ec-b4be-1250a270197b', 1, NULL, '2020-02-06 13:33:04', '2020-02-06 13:33:04'),
('f607d2a7-a744-40ec-b4be-1250a270227b', 'Toor/Arhar Dal', 'f607d2a7-a744-40ec-b4be-1250a270163b', NULL, 'f607d2a7-a744-40ec-b4be-1250a270197b', 1, NULL, '2020-02-06 13:33:04', '2020-02-06 13:33:04'),
('f607d2a7-a744-40ec-b4be-1250a270228b', 'Peas/Matar - White', 'f607d2a7-a744-40ec-b4be-1250a270163b', NULL, 'f607d2a7-a744-40ec-b4be-1250a270197b', 1, NULL, '2020-02-06 13:33:04', '2020-02-06 13:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_values`
--

CREATE TABLE `product_attribute_values` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int DEFAULT NULL,
  `float_value` double DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` json DEFAULT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_cross_sells`
--

CREATE TABLE `product_cross_sells` (
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_inventories`
--

CREATE TABLE `product_inventories` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inventory_source_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_ordered_inventories`
--

CREATE TABLE `product_ordered_inventories` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int NOT NULL DEFAULT '0',
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_relations`
--

CREATE TABLE `product_relations` (
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rating` int NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_super_attributes`
--

CREATE TABLE `product_super_attributes` (
  `product_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_up_sells`
--

CREATE TABLE `product_up_sells` (
  `parent_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `type` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verify_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `email`, `email_verified_at`, `email_verify_token`, `username`, `password`, `role_id`, `provider`, `provider_id`, `verified`, `status`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
('f607d2a7-a744-40ec-b4be-1250a270163b', 'ujjwal.test@gmail.com', '2020-01-06 02:02:44', '1', 'ujjwalbera', '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC', 1, 'web', '0', 1, 1, 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt', NULL, '2020-01-06 01:59:44', '2020-01-06 01:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_profiles`
--

CREATE TABLE `vendor_profiles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_time` time DEFAULT NULL,
  `close_time` time DEFAULT NULL,
  `close_day` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `0` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_profiles`
--

INSERT INTO `vendor_profiles` (`id`, `vendor_id`, `name`, `email`, `phone`, `slug`, `store_name`, `address`, `latitude`, `longitude`, `city`, `state`, `country`, `store_no`, `open_time`, `close_time`, `close_day`, `gst_no`, `confirmed`, `status`, `created_at`, `updated_at`, `0`) VALUES
('84f08cfe-fc4c-4b6f-88c3-8f87ca1aa72c', 'f607d2a7-a744-40ec-b4be-1250a270163b', 'Ujjwal Bera', 'ujjwal.test@gmail.com', '8981987974', 'usha-digital', 'Usha Digital Seva Kendra', 'Bonhijli, Khanakul, Hooghly', '22.634200', '87.842914', 'Hooghly', 'West Bengal', 'India', '001', '08:00:00', '20:30:00', 'Sunday', '07AAACB5343E1Z3', 1, 1, '2020-01-06 01:59:44', '2020-01-06 01:59:44', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_username_unique` (`username`),
  ADD KEY `admins_email_username_role_id_status_index` (`email`,`username`,`role_id`,`status`);

--
-- Indexes for table `admin_profiles`
--
ALTER TABLE `admin_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_profiles_admin_id_foreign` (`admin_id`),
  ADD KEY `admin_profiles_name_address_index` (`name`,`address`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_code_unique` (`code`);

--
-- Indexes for table `attribute_families`
--
ALTER TABLE `attribute_families`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_groups_attribute_family_id_name_unique` (`attribute_family_id`,`name`);

--
-- Indexes for table `attribute_group_mappings`
--
ALTER TABLE `attribute_group_mappings`
  ADD PRIMARY KEY (`attribute_id`,`attribute_group_id`),
  ADD KEY `attribute_group_mappings_attribute_group_id_foreign` (`attribute_group_id`);

--
-- Indexes for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_options_code_unique` (`code`),
  ADD KEY `attribute_options_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_filterable_attributes`
--
ALTER TABLE `category_filterable_attributes`
  ADD KEY `category_filterable_attributes_category_id_foreign` (`category_id`),
  ADD KEY `category_filterable_attributes_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_name_unique` (`name`),
  ADD UNIQUE KEY `customers_email_unique` (`email`),
  ADD UNIQUE KEY `customers_phone_unique` (`phone`),
  ADD UNIQUE KEY `customers_username_unique` (`username`),
  ADD KEY `customers_email_username_index` (`email`,`username`);

--
-- Indexes for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_address_latitude_unique` (`latitude`),
  ADD UNIQUE KEY `customer_address_longitude_unique` (`longitude`),
  ADD KEY `customer_address_customer_id_foreign` (`customer_id`),
  ADD KEY `customer_address_address_index` (`address`),
  ADD KEY `customer_address_city_index` (`city`),
  ADD KEY `customer_address_state_index` (`state`),
  ADD KEY `customer_address_country_index` (`country`),
  ADD KEY `customer_address_confirmed_index` (`confirmed`),
  ADD KEY `customer_address_status_index` (`status`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_attribute_family_id_foreign` (`attribute_family_id`);

--
-- Indexes for table `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_product_attribute_value_index_unique` (`attribute_id`,`product_id`,`vendor_id`),
  ADD KEY `product_attribute_values_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD KEY `product_categories_product_id_foreign` (`product_id`),
  ADD KEY `product_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_cross_sells`
--
ALTER TABLE `product_cross_sells`
  ADD KEY `product_cross_sells_parent_id_foreign` (`parent_id`),
  ADD KEY `product_cross_sells_child_id_foreign` (`child_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_source_vendor_index_unique` (`product_id`,`inventory_source_id`,`vendor_id`),
  ADD KEY `product_inventories_vendor_id_foreign` (`vendor_id`);

--
-- Indexes for table `product_ordered_inventories`
--
ALTER TABLE `product_ordered_inventories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_ordered_inventories_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_relations`
--
ALTER TABLE `product_relations`
  ADD KEY `product_relations_parent_id_foreign` (`parent_id`),
  ADD KEY `product_relations_child_id_foreign` (`child_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_super_attributes`
--
ALTER TABLE `product_super_attributes`
  ADD KEY `product_super_attributes_product_id_foreign` (`product_id`),
  ADD KEY `product_super_attributes_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `product_up_sells`
--
ALTER TABLE `product_up_sells`
  ADD KEY `product_up_sells_parent_id_foreign` (`parent_id`),
  ADD KEY `product_up_sells_child_id_foreign` (`child_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_name_email_type_index` (`name`,`email`,`type`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendors_email_unique` (`email`),
  ADD UNIQUE KEY `vendors_username_unique` (`username`),
  ADD KEY `vendors_email_username_index` (`email`,`username`);

--
-- Indexes for table `vendor_profiles`
--
ALTER TABLE `vendor_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_profiles_email_unique` (`email`),
  ADD UNIQUE KEY `vendor_profiles_phone_unique` (`phone`),
  ADD UNIQUE KEY `vendor_profiles_slug_unique` (`slug`),
  ADD UNIQUE KEY `vendor_profiles_latitude_unique` (`latitude`),
  ADD UNIQUE KEY `vendor_profiles_longitude_unique` (`longitude`),
  ADD UNIQUE KEY `vendor_profiles_gst_no_unique` (`gst_no`),
  ADD KEY `vendor_profiles_vendor_id_foreign` (`vendor_id`),
  ADD KEY `vendor_profiles_address_index` (`address`),
  ADD KEY `vendor_profiles_city_index` (`city`),
  ADD KEY `vendor_profiles_state_index` (`state`),
  ADD KEY `vendor_profiles_country_index` (`country`),
  ADD KEY `vendor_profiles_close_day_index` (`close_day`),
  ADD KEY `vendor_profiles_confirmed_index` (`confirmed`),
  ADD KEY `vendor_profiles_status_index` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_profiles`
--
ALTER TABLE `admin_profiles`
  ADD CONSTRAINT `admin_profiles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD CONSTRAINT `attribute_groups_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_group_mappings`
--
ALTER TABLE `attribute_group_mappings`
  ADD CONSTRAINT `attribute_group_mappings_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_group_mappings_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD CONSTRAINT `attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_filterable_attributes`
--
ALTER TABLE `category_filterable_attributes`
  ADD CONSTRAINT `category_filterable_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_filterable_attributes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `customer_address`
--
ALTER TABLE `customer_address`
  ADD CONSTRAINT `customer_address_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`) ON DELETE RESTRICT;

--
-- Constraints for table `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD CONSTRAINT `product_attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attribute_values_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_cross_sells`
--
ALTER TABLE `product_cross_sells`
  ADD CONSTRAINT `product_cross_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_cross_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD CONSTRAINT `product_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_inventories_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_ordered_inventories`
--
ALTER TABLE `product_ordered_inventories`
  ADD CONSTRAINT `product_ordered_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_relations`
--
ALTER TABLE `product_relations`
  ADD CONSTRAINT `product_relations_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_relations_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_super_attributes`
--
ALTER TABLE `product_super_attributes`
  ADD CONSTRAINT `product_super_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `product_super_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_up_sells`
--
ALTER TABLE `product_up_sells`
  ADD CONSTRAINT `product_up_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_up_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vendor_profiles`
--
ALTER TABLE `vendor_profiles`
  ADD CONSTRAINT `vendor_profiles_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
