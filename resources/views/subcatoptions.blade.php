@foreach($subcategories as $subcatoption)
    <option value="{{ $subcatoption->id }}" @if(isset($current->id) && ($current->id == $subcatoption->id)) {{ 'disabled'
     }} @endif @if(isset($current->parent_id) && ($current->parent_id == $subcatoption->id)) {{ 'selected'
     }} @endif>{{
    $child }}{{
    $subcatoption->name }}</option>
    @if(count($subcatoption->subcategories))
        @include('subcatoptions', ['subcategories' => $subcatoption->subcategories, 'child' => '-'.$child, 'current'
         => $current])
    @endif
@endforeach
