<a href="<?php echo route('admin-dashboard'); ?>" class="brand-link">
    <img src="{!! URL::asset('assets/admin/dist/img/AdminLTELogo.png') !!}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Vendorism</span>
</a>
<?php
    $details = request()->route()->getAction();

?>>
<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{!! URL::asset('assets/admin/dist/img/user2-160x160.jpg') !!}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Admin: {{ Auth::guard('admin')->user()->username }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="<?php echo route('admin-dashboard'); ?>" class="nav-link <?php if($details['as'] == 'admin-dashboard') { ?>active<?php } ?>">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-header">Product Management</li>
            <li class="nav-item has-treeview <?php if($details['as'] == 'admin-attributes') { ?>menu-open<?php } ?>">
                <a href="#" class="nav-link <?php if($details['as'] == 'admin-attributes') { ?>active<?php } ?>">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                        Attributes
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="<?php echo route('admin-attributes'); ?>" class="nav-link <?php if($details['as'] == 'admin-attributes') { ?>active<?php } ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Attributes</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo route('admin-attribute-families'); ?>" class="nav-link <?php if($details['as'] == 'admin-attribute-families') { ?>active<?php } ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Attribute Families</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo route('admin-attribute-groups'); ?>" class="nav-link <?php if($details['as'] ==
                        'admin-attribute-groups') { ?>active<?php } ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Attribute Groups</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo route('admin-attribute-options'); ?>" class="nav-link <?php if($details['as']
                        == 'admin-attribute-options') { ?>active<?php } ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Attribute Options</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview <?php if($details['as'] == 'admin-types') { ?>menu-open<?php } ?>">
                <a href="#" class="nav-link <?php if($details['as'] == 'admin-types') { ?>active<?php } ?>">
                    <i class="nav-icon fas fa-list"></i>
                    <p>Products
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="<?php echo route('admin-products'); ?>" class="nav-link <?php if($details['as'] == 'admin-products') { ?>active<?php } ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Products</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="<?php echo route('admin-categories'); ?>" class="nav-link <?php if($details['as'] == 'admin-categories') { ?>active<?php } ?>">
                    <i class="nav-icon fas fa-th-list"></i>
                    <p>Categories</p>
                </a>
            </li>

            <li class="nav-header">Site Users</li>
            <li class="nav-item">
                <a href="{{ route('admin-customers') }}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Customers</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin-vendors') }}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Vendors</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin-carriers') }}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Carriers</p>
                </a>
            </li>

            <li class="nav-header">Order</li>
            <li class="nav-item">
                <a href="{{ route('admin-orders') }}" class="nav-link">
                    <i class="nav-icon fas fa-cart-plus"></i>
                    <p>Orders</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file-pdf"></i>
                    <p>Invoices</p>
                </a>
            </li>


            <li class="nav-header">&nbsp;</li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
