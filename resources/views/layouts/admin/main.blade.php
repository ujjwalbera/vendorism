<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/fontawesome-free/css/all.min.css') !!}">
        <!-- Ionicons -->
        <!--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') !!}">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/jqvmap/jqvmap.min.css') !!}">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css') !!}">

        <!-- Select2 -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/select2/css/select2.min.css') !!}">
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') !!}">
        <!-- Bootstrap4 Duallistbox -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') !!}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/dist/css/adminlte.min.css') !!}">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/daterangepicker/daterangepicker.css') !!}">
        <!-- summernote -->
        <link rel="stylesheet" href="{!! URL::asset('assets/admin/plugins/summernote/summernote-bs4.css') !!}">
        <!-- Google Font: Source Sans Pro -->
        <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->

        <!-- jQuery -->
        <script src="{!! URL::asset('assets/admin/plugins/jquery/jquery.min.js') !!}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{!! URL::asset('assets/admin/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{!! URL::asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                @include('layouts.admin.partials.header')
            </nav>

            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                @include('layouts.admin.partials.sidebar')
            <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">Dashboard</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard v1</li>
                                </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                @include('layouts.admin.partials.footer')
            </footer>

        </div>
        <!-- ./wrapper -->

        <!-- ChartJS -->
        <script src="{!! URL::asset('assets/admin/plugins/datatables/jquery.dataTables.js') !!}"></script>
        <!-- ChartJS -->
        <script src="{!! URL::asset('assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') !!}"></script>

        <!-- ChartJS -->
        <script src="{!! URL::asset('assets/admin/plugins/chart.js/Chart.min.js') !!}"></script>
        <!-- Sparkline -->
        <script src="{!! URL::asset('assets/admin/plugins/sparklines/sparkline.js') !!}"></script>
        <!-- JQVMap -->
        <script src="{!! URL::asset('assets/admin/plugins/jqvmap/jquery.vmap.min.js') !!}"></script>
        <script src="{!! URL::asset('assets/admin/plugins/jqvmap/maps/jquery.vmap.usa.js') !!}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{!! URL::asset('assets/admin/plugins/jquery-knob/jquery.knob.min.js') !!}"></script>

        <!-- Select2 -->
        <script src="{!! URL::asset('assets/admin/plugins/select2/js/select2.full.min.js') !!}"></script>
        <!-- Bootstrap4 Duallistbox -->
        <script src="{!! URL::asset('assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') !!}"></script>
        <!-- InputMask -->
        <script src="{!! URL::asset('assets/admin/plugins/moment/moment.min.js') !!}"></script>
        <script src="{!! URL::asset('assets/admin/plugins/inputmask/min/jquery.inputmask.bundle.min.js') !!}"></script>

        <!-- daterangepicker -->
        <script src="{!! URL::asset('assets/admin/plugins/moment/moment.min.js') !!}"></script>
        <script src="{!! URL::asset('assets/admin/plugins/daterangepicker/daterangepicker.js') !!}"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{!! URL::asset('assets/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
        <!-- Summernote -->
        <script src="{!! URL::asset('assets/admin/plugins/summernote/summernote-bs4.min.js') !!}"></script>
        <!-- overlayScrollbars -->
        <script src="{!! URL::asset('assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! URL::asset('assets/admin/dist/js/adminlte.js') !!}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{!! URL::asset('assets/admin/dist/js/pages/dashboard.js') !!}"></script>
        <!-- AdminLTE for demo purposes -->
        <!--<script src="{!! URL::asset('assets/admin/dist/js/demo.js') !!}"></script>-->
        <script>
            $(function () {
                if ( $( "#datatable" ).length ) {
                    $('#datatable').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                    });
                }
                if ( $( ".select2" ).length ) {
                    $('.select2').select2();
                }
            });
        </script>
        <script>
            /*$(function () {
                $('.statusbutton').click(function () {
                    alert($(this).attr('href'));
                });
            });
            $('.statusbutton').click(function (evt) {
                evt.preventDefault();
                var link = $(this).attr('href');
                $.ajax({
                    type: "GET",
                    url: '/property_details',
                    dataType: "json",
                    data: {
                        href: link
                    }
                }).done(function (data) {
                    // I'm assuming that the 'property_details' table has a 'url' field?
                    // If not, just replace this with whatever you need.
                    if (data && data.hasOwnProperty('url')) {
                        window.location = data.url;
                    }
                });
            });*/
        </script>
    </body>
</html>
