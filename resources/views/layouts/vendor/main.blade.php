<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::asset('assets/vendor/images/favicon.png') !!}">
    <title>Vendor | Dashboard</title>
    <!-- Custom CSS -->
    <link href="{!! URL::asset('assets/vendor/libs/flot/css/float-chart.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/vendor/libs/select2/dist/css/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/vendor/libs/jquery-minicolors/jquery.minicolors.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/vendor/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/vendor/libs/quill/dist/quill.snow.css') !!}" rel="stylesheet">

    <link href="{!! URL::asset('assets/vendor/extra-libs/multicheck/multicheck.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('assets/vendor/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') !!}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{!! URL::asset('assets/vendor/dist/css/style.min.css') !!}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>-->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--[endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin5">
        @include('layouts.vendor.partials.header')
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin5">
        <!-- Sidebar scroll-->
    @include('layouts.vendor.partials.sidebar')
    <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            @include('layouts.vendor.partials.breadcrumb')
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            @yield('content')
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer text-center">
            @include('layouts.vendor.partials.footer')
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{!! URL::asset('assets/vendor/libs/jquery/dist/jquery.min.js') !!}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{!! URL::asset('assets/vendor/libs/popper.js/dist/umd/popper.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/extra-libs/sparkline/sparkline.js') !!}"></script>
<!--Wave Effects -->
<script src="{!! URL::asset('assets/vendor/dist/js/waves.js') !!}"></script>
<!--Menu sidebar -->
<script src="{!! URL::asset('assets/vendor/dist/js/sidebarmenu.js') !!}"></script>
<!--Custom JavaScript -->
<script src="{!! URL::asset('assets/vendor/dist/js/custom.min.js') !!}"></script>
<!--This page JavaScript -->
<!-- <script src="{!! URL::asset('assets/vendor/dist/js/pages/dashboards/dashboard1.js') !!}"></script> -->
<!-- Charts js Files -->
<script src="{!! URL::asset('assets/vendor/libs/flot/excanvas.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot/jquery.flot.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot/jquery.flot.pie.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot/jquery.flot.time.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot/jquery.flot.stack.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot/jquery.flot.crosshair.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/dist/js/pages/chart/chart-page-init.js') !!}"></script>

<script src="{!! URL::asset('assets/vendor/extra-libs/multicheck/datatable-checkbox-init.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/extra-libs/multicheck/jquery.multicheck.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/extra-libs/DataTables/datatables.min.js') !!}"></script>

<script src="{!! URL::asset('assets/vendor/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/dist/js/pages/mask/mask.init.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/select2/dist/js/select2.full.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/select2/dist/js/select2.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/jquery-asColor/dist/jquery-asColor.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/jquery-asGradient/dist/jquery-asGradient.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/jquery-minicolors/jquery.minicolors.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}"></script>
<script src="{!! URL::asset('assets/vendor/libs/quill/dist/quill.min.js') !!}"></script>

<script>
    /****************************************
     *       Basic Table                   *
     ****************************************/
    $('#datatable').DataTable();

    //***********************************//
    // For select 2
    //***********************************//
    $(".select2").select2();

    /*datwpicker*/
    jQuery('.mydatepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    var quill = new Quill('#editor', {
        theme: 'snow'
    });

</script>
</body>
</html>
